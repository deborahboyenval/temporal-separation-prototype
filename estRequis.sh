#!/usr/bin/env bash


################ Function definitions: ################ 

function get_var {
echo $(tr -d "[+-]" <<< $1) 
}

function get_var_state {
# get the state of a variable ($2) in a given state ($1).
cut -d "=" -f2 <<< $(grep -o "$2=[0-9]" <<< $1)
}

function get_previous_state {
var=$(get_var $2)
sign=$(tr -d "[A-Za-z]*" <<< $2) 
val=$(cut -d "=" -f2  <<< $(grep -o "$var=[[:digit:]]" <<< $1))
if [ $sign == "+" ];
	then 
	new_val=$(($val-1))
elif [ $sign == "-" ];
	then 
	new_val=$(($val+1))
fi
echo $(sed "s/$var=$val/$var=$new_val/g" <<< $1)
#$1 is EtaNC
#$2 is an event which is able to end a phase
}

################ Util remarks: ################ 
# ${string%?} : delete the last character of the string. 
# WARNING : csvtool is required ! Installation: sudo apt-get install csvtool

################ Some Assignments: ################ 
file_path="./TotemFiles" #smbionet .out file

Eta1=$1
Eta0=$(get_previous_state $1 $2)
Y=$(get_var $3)
Eta1Y=$(get_var_state $Eta1 $Y)

K_v1_omega_Eta0=$(./findApplicableK.sh $Eta0 $Y)
K_v1_omega_Eta1=$(./findApplicableK.sh $Eta1 $Y)

PermC=$4
PermI=$5

################ Main: ################
sed -i "s/;/,/g" $file_path/input.csv #csvtools functions with a comma separator. No check for semicolon.

# storage of the unsubstitued constraint. 
constraintNotSubst="(($K_v1_omega_Eta1 - $Eta1Y) x ($K_v1_omega_Eta0 - $Eta1Y)) <= 0"

oneM_satisfied='false' # by defaut false because evaluation of the disjunction of contraint substitution. Becomes true if one model satisfy the constraint (see below).

echo '' > estRequis.txt # the file that stores the evaluation of the constraint. By default empty is true. If one constraint, built from a permutation, is satisfied by no model then estRequis is False.

constraint=""
csvtool namedcol $K_v1_omega_Eta1,$K_v1_omega_Eta0 $file_path/input.csv > perm_models.txt 2>/dev/null

models=`csvtool drop 1 perm_models.txt | sort -u` 
models=`tr '\n' ' ' <<< $models`

for model in $models
do
	model=`tr -d ' ' <<< $model`
	K1=`cut -d "," -f1 <<< $model`
	K0=`cut -d "," -f2 <<< $model`
	s1=$(($K1-$Eta1Y))
	s0=$(($K0-$Eta1Y)) 
	res=$(($s0*$s1))
	if [[ $res -le 0 ]] # /!\ if the constraint is valid for AT LEAST ONE MODEL THEN estRequis is TRUE. /!\
		then 
		constraint="$constraint($K_v1_omega_Eta1 = $K1 and $K_v1_omega_Eta0 = $K0)|"
		oneM_satisfied='true'
		echo 'true' > estRequis.txt #returns true if one model satisfies one constraint.
	fi
done

#oneM_satisfied : a permutation is "satisfied" if one model satisfies it. If one constraint is satisfied by no model then estRequis is false (Forall X and Y so Forall permutation, estRequis has to be true).



echo "$PermC;$PermI : $constraintNotSubst : $oneM_satisfied" >> ./OutputFiles/estRequisNS.out

#if input.out is empty then "" > estRequis.txt and so false. 
#if input.out is not empty and the contraint is not valid then "" > estRequis.txt and so false. 

estRequisFile=$(cat ./OutputFiles/estRequis.out)
#for each permutation tested, the constraint is cumulated within estRequis.out. 

# if estRequis is True : storage of all constraints with valid substitution of K1 and K0 : IDENTIFICATION OF PARAMETERS
if [ "$constraint" != '' ]
then
	if [ "$estRequisFile" == '' ]
	then 	
		echo "$PermC;$PermI : (${constraint%?})" >> ./OutputFiles/estRequis.out
	else
		constraintOut=$(grep -o "(${constraint%?})" < ./OutputFiles/estRequis.out)
		if [ "$constraintOut" == '' ] #duplicates are not stored. 
		then
			echo "" >> ./OutputFiles/estRequis.out
			echo "$PermC;$PermI : (${constraint%?})" >> ./OutputFiles/estRequis.out
		else
			echo "$PermC;$PermI : same as below" >> ./OutputFiles/estRequis.out
		fi
	fi 
fi

# if the execution of totembionet with the current Perm returns a not satisfied WP then input.csv is not reinitialized as an empty file.
# So reinitialisation of the csv input file: 

#echo '' > $file_path/input.csv

#If the current Perm is not correct then input.csv remains empty. Else it is replaced by the value of input.csv and once used it is reinitialized. 

################ Test ################

#./estRequis.sh "sk=2,ep=0,a=0,b=0,en=0,gf=1" "sk+" "a+" "sk+;en-;sk+" "a+;sk-;sk-"

