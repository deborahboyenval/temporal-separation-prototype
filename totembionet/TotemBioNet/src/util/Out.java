package util;

import java.io.*;


/**
 * @author Adrien Richard
 */


public class Out {

    private static String previous = "";
    private static BufferedWriter w;
    private static int verb;

    //Ecriture dans le fichier de sortie
    public static void pfln() throws IOException {
        w.write("\n");
    }
    public static void pfln(String s) throws IOException {
        w.write(s + "\n");
    }
    public static void pf(String s) throws IOException {
        w.write(s);
    }

    //Ecriture dans le shell et le fichier
    public static void psfln() throws IOException {
        psfln("");
    }
    public static void psfln(String s) throws IOException {
        System.out.println(s);
        pfln(s);
    }
    public static void psf(String s) throws IOException {
        System.out.print(s);
        pf(s);
    }

    //Ecriture que le shell
    public static void ps(String s) throws IOException {
        System.out.print(s);
    }

    //Ecriture dans le shell avec retour du chariot

    public static void pr() {
        String white = "";
        for (int i = 0; i < previous.length(); i++)
            white += " ";
        System.out.print("\r" + white + "\r");
        previous = "";
    }

    public static void pr(String s) {
        String white = "";
        for (int i = 0; i < previous.length(); i++)
            white += " ";
        System.out.print("\r" + white + "\r" + s);
        previous = s;
    }

    //Gestion des variables

    public static void setVerb(int v) {
        verb = v;
    }

    public static int verb() {
        return verb;
    }

    public static void setOutputFile(String file) throws IOException {
        w = new BufferedWriter(new FileWriter(new File(file)));
    }

    public static void close() throws IOException {
        w.close();
    }

    // à utiliser en mode debug pour savoir d'où vient le message
    public static void printForDebug(String className,String message) {
        System.out.println(className + " =====> " + message);
    }

}