package back.run;

import back.jclock.Clock;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import back.search.FormalSearchEngine;
import back.search.InstanciatedSearchEngine;
import back.search.InstanciatedSearchEngineWithHoare;
import back.search.ParasAsArray;

import front.smbParser.SMBParser;
import front.smbParser.Visitor;

import util.Out;

import java.io.*;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class Run {

    enum SearchType {
        FORMAL, HOARES, INSTANCIATED, ALL;
    }

    enum Command {
        MODELS,PARAS,SIMU,JSON,DEBUG;
    }

    //Pour l'affichage toutes les secondes
    protected static class Banner extends TimerTask {

        SearchType searchType;

        public Banner(SearchType st) {
            this.searchType=st;
        }
        public void run() {
            String searchStat="";
            switch (searchType) {
                case FORMAL:{searchStat= FormalSearchEngine.stats();break;}
                case HOARES:{searchStat= InstanciatedSearchEngineWithHoare.stats();break;}
                case INSTANCIATED:{searchStat= InstanciatedSearchEngine.stats();break;}
            }
            Out.pr("> (" + c + ") " + searchStat);
        }
    }

    private static Clock c;
    private List<String> options;
    private int verbose;
    private Command command;
    private String name;
    private String path;
    private Visitor SMBVisitor;
    private Net net;

    public Run(List<String> opts, String path,String name) throws Exception{
        this.options=opts;
        this.name = name;
        this.path = path;
        this.verbose= getOption("-verbose",0);
        this.command = Command.MODELS;
        if (optionExists("-paras"))
            this.command = Command.PARAS;
        else if (optionExists("-json"))
            this.command = Command.JSON;
        else if (optionExists("-simu"))
            this.command = Command.SIMU;
        else if (optionExists("-debug"))
            this.command = Command.DEBUG;
        SMBParser smbParser = new SMBParser(path,name,verbose);
        this.SMBVisitor = smbParser.parse();

        String filePrefix = path+name;
        String outputFile = (optionExists("formal"))?filePrefix+"_formal":filePrefix;
        //Fichier de sortie
        Out.setOutputFile(getOption("-o", outputFile + ".out"));
        //Niveau d'écriture
        Out.setVerb(verbose);

        this.net = new Net(SMBVisitor, path, name,command==Command.PARAS);
    }

    private SearchType getSearchType() {
        if (net.needDynamicHoare())
            return SearchType.HOARES;
        if (options.contains("-formal"))
            return SearchType.FORMAL;
        return SearchType.INSTANCIATED;
    }

    // utilisé pour énumérer les modèles quand il n'y a pas de propriétés
    // inutile de passer par NuSMV
    // ici on écrit dans un csv
    private void writeModelsInCSV() throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(new File(path+ name +".csv")));
        String s = "n°;";
        // paramètres
        for (Gene g : net.genes()) {
            if ((!net.isEnvVar(g))) {
                for (Para p : g.paras()) {
                    s += p.getSMBname() + ";";
                }
            }
        }
        w.write(s+"\n");
        int n = 1;
        ParasAsArray.init(net);
        net.firstParameterization();
        byte[] model = ParasAsArray.currentModelAsArray();
        w.write(n+";"+ParasAsArray.getParasInCSV(model)+"\n");
        n+=1;
        while (net.nextParameterization()) {
            model = ParasAsArray.currentModelAsArray();
            w.write(n+";"+ParasAsArray.getParasInCSV(model)+"\n");
        }
        w.close();
    }

    // quand il n'y a pas de formules CTL, on affiche seulement
    // les paramétrisations possibles donc en accord avec les domaines,
    // le bloc PARA et le bloc HOARE s'il existe.
    public void enumerateModels(boolean csv) throws Exception{
        Out.psfln();
        Out.psfln("########### No CTL formula, Starting model enumeration ###########");
        Out.psfln("# Total number of models " + net.NB_PARAMETERIZATIONS);
        Out.psfln("# Computation time: " + c);
        Out.psfln("###############################################");
        if (csv) {
            Out.psfln("Models have been written in " + path+ name + ".csv");
            writeModelsInCSV();
        }
        else {
            ParasAsArray.init(net);
            net.firstParameterization();
            byte[] model = ParasAsArray.currentModelAsArray();
            int n=1;
            ParasAsArray.writeDetailedModel(model,true,n++);
            while (net.nextParameterization()) {
                model = ParasAsArray.currentModelAsArray();
                ParasAsArray.writeDetailedModel(model,true,n++);
            }
        }
    }

    public void run() throws Exception {

        switch (command) {
            case PARAS: {
                net.printForbiddenParas();
                System.out.println("######################");
                net.printParameterSet();
                break;
            }
            case SIMU : {
                net.print();
                Simu.run(net);
                break;
            }
            case JSON : {
                net.print();
                SMBVisitor.generateJSON(path + name + ".json");
                break;
            }
            case DEBUG : {
                NuSMV.runDebug(net, path + name,  optionExists("-formal"));
                break;
            }
            default : {
                net.print();
                NuSMV.setNuSMVPath();
                boolean dynamic = options.contains("-dynamic");
                boolean inversion = options.contains("-inversion");

                c = new Clock();
                Timer timer = new Timer();
                timer.schedule(new Banner(getSearchType()), 0, 1000);

                boolean writeCSV = optionExists("-csv");

                if (net.hasTemporalLogicProp()) {
                    NuSMV.run(net, path + name, dynamic, inversion, optionExists("-formal"), writeCSV);
                    deleteFiles(path,name);
                }
                else
                    enumerateModels(writeCSV);
                Out.close();
                System.exit(0);
            }

        }

    }

    // sans le mode debug, on efface les fichiers intermédiaires
    private void deleteFiles(String path, String name) {
        File dir = new File(path);
        if (verbose<2) { // on efface tout
            System.out.println("\n\nDeleting unnecessary files ...");
            //Delete all files except inputPrefix.out
            for (File file : dir.listFiles()) {
                String fileName = file.getName();
                if ("hoare".equals(fileName) && file.isDirectory()) {
                    System.out.println("     Deleting directory " + path+fileName);
                    file.delete();
                } else if (fileName.equals(name + ".smv")) {
                    System.out.println("     Deleting file " + path + fileName);
                    file.delete();
                }
            }
        }
    }

    //Option s avec un entier en argument
    private int getOption(String s, int def) throws Exception {
        int i = options.indexOf(s);
        if (i >= 0) {
            if (i + 1 < options.size())
                return Integer.valueOf(options.get(i + 1));
            throw new Exception("option " + s + " needs an int as arg");
        }
        return def;
    }

    //Option avec une chaine de caract�res en argument
    private String getOption(String s, String def) throws Exception {
        int i = options.indexOf(s);
        if (i >= 0) {
            if (i + 1 < options.size() && !options.get(i + 1).startsWith("-"))
                return options.get(i + 1);
            throw new Exception("option " + s + " needs a string as arg");
        }
        return def;
    }

    //Présence d'une option
    private boolean optionExists(String s) {
        return options.contains(s);
    }


}
