package back.jlist;

public interface ObjectWithStringID {
    public String stringId();
}
