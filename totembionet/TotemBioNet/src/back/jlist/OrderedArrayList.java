package back.jlist;

import java.util.*;

/**
 * classe pour ordonner les multiplexes dans les noms des paramètres
 * un paramètre sera de la forme K_g+x1+x2...+xi où g est le gène
 * et x1 à xi les multiplexes classés par ordre croissant
 *
 * @param <E> : String
 * @author Adrien Richard
 */

public class OrderedArrayList<E extends ObjectWithStringID> extends ArrayList<E> {

    //Retourne l'�l�ment de description s    

    public E getWithStringId(String s) {
        for (int i = 0; i < size(); i++) {
            //System.out.println(getWithStringId(i).toString() + " " + s);
            if (get(i).stringId().equals(s)) {
                //System.out.println("true");
                return get(i);
            }
        }
        return null;
    }

    //Indique si la liste this est une sous-liste de l

    public boolean sublist(List<E> l) {
        for (int i = 0; i < size(); i++)
            if (!l.contains(get(i)))
                return false;
        return true;
    }

    //Indique si la liste this et une stricte sous-liste de l

    public boolean strictSublist(List<E> l) {
        return sublist(l) && size() < l.size();
    }

    //Indique si la liste contient des �l�ments de m�me description

    public boolean hasRedundancy() {
        for (int i = 0; i < size(); i++)
            for (int j = i + 1; j < size(); j++)
                if (get(i).toString().equals(get(j).toString()))
                    return true;
        return false;
    }

    //Addition d'un �l�ment dans l'ordre alphab�tique

    public void addAlpha(E e) {
        int i;
        for (i = 0; i < size(); i++)
            if (get(i).toString().compareTo(e.toString()) >= 0) {
                add(i, e);
                break;
            }
        if (i >= size())
            add(e);
    }


}
