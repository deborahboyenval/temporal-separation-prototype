package back.net;

public interface ParaEnumerator {

    void firstParameterization();

    boolean nextParameterization();
}
