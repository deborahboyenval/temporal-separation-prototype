package back.net;

import back.jlist.ObjectWithStringID;
import back.jlogic.*;
import back.jlogic.Var;

import java.util.Objects;

/**
 * @author Adrien Richard
 */


public class Reg implements ObjectWithStringID{

    //Nom de la r�gulation
    String name;
    //Formule bool�enne (pr�sence/absence de la r�gulation)
    Formula formula;

    //Constructeur
    public Reg(String name, Formula formula) throws Exception {
        this.name = name;
        this.formula = formula;
        if (formula instanceof Var)
            throw new Exception("The formula connot be reduced to a variable");
    }

    //La description
    public String toString() {
        return name;
    }

    // description format SMB sans la cible
    public String toSMBString() {
        return name + " [" + formula + "]=>";
    }

    //Indique si la formule est pr�sente
    public boolean isEffective() {
        return formula.eval() > 0;
    }

    /////////////////////////////////////////////////////
    // utilisé pour les OrderedArrayList

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Reg reg = (Reg) o;
//        return Objects.equals(name, reg.name) &&
//                Objects.equals(formula, reg.formula);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(name, formula);
//    }

    @Override
    public String stringId() {
        return name;
    }
}