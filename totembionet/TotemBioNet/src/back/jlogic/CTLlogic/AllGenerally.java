package back.jlogic.CTLlogic;

import back.jlogic.Formula;
import back.jlogic.blogic.Not;

/**
 * classe pour représenter l'opérateur CTL AG
 * *
 *
 * @author Hélène Collavizza
 */


public class AllGenerally extends UnaryCTLFormula {

    public AllGenerally(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.AG + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new ExistGenerally(getLeft().negate());
    }
    */

    @Override
    public Formula fairBio() {
        return new AllGenerally(getLeft().fairBio());
    }

}
