package back.jlogic.CTLlogic;

import back.jlogic.Formula;
import back.jlogic.blogic.And;
import back.jlogic.blogic.Not;

/**
 * classe pour représenter l'opérateur CTL AU
 *
 * @author Hélène Collavizza
 */

public class AllUntil extends BinaryCTLFormula {

    public AllUntil(Formula l, Formula r) {
        super(l, r);
    }

    @Override
    public String toString() {
        return "A [" + getLeft() + " U " + getRight() + "]";
    }

    @Override
    public Formula fairBio() {
        Formula fairR = getRight().fairBio();
        Formula fairL = getLeft().fairBio();
        Formula f1 = new AllFuture(fairR);
        Formula notR = new Not(fairR);
        Formula f2 = new ExistUntil(notR, new And(new Not(fairL), notR));
        return new And(f1.fairBio(), new Not(f2.fairBio()));
    }

}
