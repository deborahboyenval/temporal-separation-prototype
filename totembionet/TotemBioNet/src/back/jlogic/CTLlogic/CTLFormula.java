package back.jlogic.CTLlogic;

import back.jlogic.Formula;

import java.util.HashMap;
import java.util.List;

/**
 * classe pour représenter les formules CTL
 * les méthodes abstraites de Formula sont inutiles ici
 *
 * @author Hélène Collavizza
 */


public abstract class CTLFormula extends Formula {

    // on ne sait pas évaluer une formule CTL
    public int eval() {
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        return 0;
    }

    @Override
    public List<Formula> toSetOfConjunct() {
        return null;
    }


}
