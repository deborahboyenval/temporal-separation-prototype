package back.jlogic.blogic;

import back.jlogic.Formula;

import java.util.HashMap;

/**
 * @author Adrien Richard
 * @author helen
 */

public class Add extends IntegerFormula {

    public Add(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        return getLeft().eval() + getRight().eval();
    }

    public int eval(HashMap<String, Integer> paraState) {
        return getLeft().eval(paraState) + getRight().eval(paraState);
    }

    public String toString() {
        return "(" + getLeft() + Formula.ADD + getRight() + ")";
    }

}