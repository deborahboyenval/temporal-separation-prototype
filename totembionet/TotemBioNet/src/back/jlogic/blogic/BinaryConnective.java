package back.jlogic.blogic;

import back.jlogic.Formula;
import back.jlogic.Var;

/**
 * @author Adrien Richard
 */

abstract public class BinaryConnective extends Formula {

    private Formula right;
    private Formula left;

    public BinaryConnective(Formula left, Formula right) {
        this.left = left;
        this.right = right;
    }

    public Formula getLeft() {
        return left;
    }

    public Formula getRight() {
        return right;
    }

    public static Formula makeFormula(String f, Formula l, Formula r) {
        if (f.equals(EQUI))
            return new Equi(l, r);
        if (f.equals(IMPLY))
            return new Imply(l, r);
        if (f.equals(OR))
            return new Or(l, r);
        if (f.equals(AND))
            return new And(l, r);
        if (f.equals(LESS_EQ))
            return new LessEq(l, r);
        if (f.equals(LESS))
            return new Less(l, r);
        if (f.equals(GREAT_EQ))
            return new GreatEq(l, r);
        if (f.equals(GREAT))
            return new Great(l, r);
        if (f.equals(DIFF))
            return new Diff(l, r);
        if (f.equals(EQUA))
            return new Equa(l, r);
        if (f.equals(SOUS))
            return new Sous(l, r);
        if (f.equals(ADD))
            return new Add(l, r);
        if (f.equals(DIV))
            return new Div(l, r);
        if (f.equals(MULT))
            return new Mult(l, r);
        return null;
    }

    public void set(String name, int level) {
        left.set(name, level);
        right.set(name, level);
    }

    public boolean insert(Var v) {
        boolean leftInsert, rightInsert;
        if ((left instanceof Var) && ((Var) left).name.equals(v.name)) {
            left = v;
            leftInsert = true;
        } else
            leftInsert = left.insert(v);
        if ((right instanceof Var) && ((Var) right).name.equals(v.name)) {
            right = v;
            rightInsert = true;
        } else
            rightInsert = right.insert(v);
        return leftInsert || rightInsert;
    }

}