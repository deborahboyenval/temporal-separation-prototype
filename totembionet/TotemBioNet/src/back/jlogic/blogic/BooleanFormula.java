package back.jlogic.blogic;

import back.jlogic.Formula;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public abstract class BooleanFormula extends BinaryConnective {

    public BooleanFormula(Formula l, Formula r) {
        super(l, r);
    }

    public Formula negate() {
        return new Not(this);
    }

}
