package back.jlogic.blogic;

import back.jlogic.Formula;

import java.util.HashMap;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class Div extends IntegerFormula {

    public Div(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        return getLeft().eval() / getRight().eval();
    }

    public int eval(HashMap<String, Integer> paraState) {
        return getLeft().eval(paraState) / getRight().eval(paraState);
    }

    public String toString() {
        return "(" + getLeft() + Formula.DIV + getRight() + ")";
    }

}