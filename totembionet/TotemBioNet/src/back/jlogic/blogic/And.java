package back.jlogic.blogic;

import back.jlogic.Formula;
import util.TotemBionetException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class And extends BooleanFormula {

    public And(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() > 0 && getRight().eval() > 0)
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) > 0 && getRight().eval(state) > 0)
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.AND + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new And(getLeft().fairBio(), getRight().fairBio());
    }

//    @Override
//    public Formula negate() {
//        if (!(getLeft() instanceof Imply) && !(getRight() instanceof Imply))
//            return super.negate();
//        if (getLeft() instanceof Imply)
//            ((Imply) getLeft()).setRoot();
//        if (getRight() instanceof Imply)
//            ((Imply) getRight()).setRoot();
//        return new Or(getLeft().negate(),getRight().negate());
//    }

    // @author Hélène Collavizza
    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        ArrayList<Formula> conjonct = new ArrayList<Formula>();
        conjonct.addAll(getRight().toSetOfConjunct());
        conjonct.addAll(getLeft().toSetOfConjunct());
        return conjonct;
    }
}