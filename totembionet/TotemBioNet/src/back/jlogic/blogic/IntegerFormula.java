package back.jlogic.blogic;

import back.jlogic.Formula;
import util.TotemBionetException;

import java.util.List;


/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public abstract class IntegerFormula extends BinaryConnective {

    public IntegerFormula(Formula l, Formula r) {
        super(l, r);
    }

    public Formula negate() {
        return this;
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Integer expression is not a comparator");
    }

}
