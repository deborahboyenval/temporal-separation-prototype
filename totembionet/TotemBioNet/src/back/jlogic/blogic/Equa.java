package back.jlogic.blogic;

import back.jlogic.Formula;
import back.net.Para;
import util.Out;
import util.TotemBionetException;

import java.util.HashMap;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class Equa extends Comparator {

    public Equa(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() == getRight().eval())
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        //System.out.println(this + " " +  (getLeft().eval(state) == getRight().eval(state)));
        if (getLeft().eval(state) == getRight().eval(state))
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.EQUA + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    // on suppose que c'est de la forme (var == val)
    public void setParaValue(Para p) throws TotemBionetException {
        String smbName = getLeft().toString();
        int val = getRight().eval();
        int pValMax = p.maxLevel();
        int pValMin = p.minLevel();
        if (smbName.equals(p.getSMBname())) {
            if (p.fixe & ((pValMax!=val) | (pValMin!=val)))
                throw new TotemBionetException("No models. PARA and HOARE values are conflicting for parameter " + smbName);
            if (val >= pValMin & val <= pValMax) {
                p.setMinMax(val, val);
                if (Out.verb()>=1){
                    System.out.println("      Old domain: " + pValMin + ".." + pValMax +", New domain: " + p.minLevel() + ".." + p.maxLevel());
                }
            } else {
                throw new TotemBionetException("No models. Hoare constraints are conflicting for parameter " + smbName);
            }
        }

    }

}