package back.jlogic.blogic;

import back.jlogic.Formula;
import back.net.Para;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hélène Collavizza
 */
public abstract class Comparator extends BooleanFormula {


    public Comparator(Formula l, Formula r) {
        super(l, r);
    }

    @Override
    public List<Formula> toSetOfConjunct() {
        ArrayList<Formula> f = new ArrayList<Formula>();
        f.add(this);
        return f;
    }

    public abstract void setParaValue(Para p) throws Exception;

}
