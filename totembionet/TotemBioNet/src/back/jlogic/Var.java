package back.jlogic;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author helen
 */

public class Var extends Formula {

    //nom de la variable
    public final String name;
    //niveau courant de la variable
    public int level;

    //construction avec initialisation finale du nom

    public Var(String name) {
        this.name = name;
    }

    //niveau courant

    public int getLevel() {
        return level;
    }

    //determination du niveau

    public void setLevel(int level) {
        this.level = level;
    }


    //METHODES ABSTRAITES DE FORMULA

    public int eval() {
        return level;
    }

    public int eval(HashMap<String, Integer> state) {
        if (state.containsKey(name)) {
            return state.get(name);
        }
        System.err.println("Unknown variable in this environment " + name);
        return level;
    }

    @Override
    public Formula negate() {
        return this;
    }

    @Override
    public List<Formula> toSetOfConjunct() {
        return null;
    }

    @Override
    public Formula fairBio() {
        return this;
    }


    public String toString() {
        return name;
    }

    public boolean insert(Var v) {
        return false;
    }

    public void set(String name, int level) {
        if (this.name.equals(name))
            this.level = level;
    }


}