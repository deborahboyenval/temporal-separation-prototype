package back.search;

import back.net.Para;

import java.util.HashMap;
import java.util.Map;

/**
 * classe pour représenter un pattern qui n'est pas satisfiable
 * associe un byte et les bornes min des variables libres pour lesquelles
 * le pattern était insatisfiable
 * Exemple : pattern 10-112- est insatisfiable si la 1ère variable est >=1 et la 2d >=0
 *
 * @author Hélène Collavizza
 */

public class SolvedPattern {
    private byte[] pattern;
    private HashMap<Integer, Integer> freeParasMinValues;

    public SolvedPattern(byte[] pattern) {
        this.pattern = pattern.clone();
        freeParasMinValues = new HashMap<Integer, Integer>();
        for (Map.Entry<Integer, Para> e : ParasAsArray.formalParas.entrySet()) {
            freeParasMinValues.put(e.getKey(), e.getValue().currentMin());
        }
    }

    /**
     * Pour savoir si le modèle courant match un pattern.
     * Il faut que les valeurs instanciées soient OK et que le modèle soit dans le domaine
     * du pattern
     */
    public boolean match(byte[] model) {
        for (int i = 1; i < pattern.length; i++) {
            if ((pattern[i] != -1) && (pattern[i] != model[i]))
                return false;
            if ((pattern[i] == -1) && model[i] < freeParasMinValues.get(i)) {
//                System.out.println("PAS MATCH !!!");
                return false;
            }
        }

//        try {
//            System.out.println("match");
//            ParasAsArray.printModel(model);
//            ParasAsArray.printModel(pattern);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return true;
    }

    public void writeDetailled() throws Exception {
        ParasAsArray.writeDetailedPattern(pattern, freeParasMinValues);
    }

}
