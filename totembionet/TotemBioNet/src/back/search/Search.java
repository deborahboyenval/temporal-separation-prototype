package back.search;

import back.jclock.Clock;

public interface Search {
    void doSearch(String name, boolean csv) throws Exception;

    void writeSearchInCSV(Clock c, String file) throws Exception;

    void writeSearch(Clock c) throws Exception;

    boolean hasNext();

    void initSearch(String name) throws Exception;
}
