// Grammaire des formules logiques issues de Hoare-Fol
//
//           Author: Sokhna SECK [ndeye-sokhna.seck@outlook.fr]


grammar HoareFol;

root          : '(' rootMember (BOOL_OP rootMember)+ ')'
              ;

rootMember    : '(' expr ')' | 'false' ;

expr          : expr BOOL_OP expr               # expr_bool_op
              | NEG '(' expr ')'                # expr_neg
              |'(' expr ')'                     # expr_brackets
              | (KID|ID) COMP NUM               # expr_atome
              | 'true'                          # expr_true
              | 'false'                         # expr_false
              ;

WS          : (' '|'\r'|'\n'|'\t')+  -> skip;
COMMENT     : '#' ~('\n')*    -> skip;

KID         : 'K_'[a-zA-Z][a-zA-Z0-9_]*(':'[a-zA-Z][a-zA-Z0-9_]*)*;
ID          : [a-zA-Z][a-zA-Z0-9_]*;
NUM         : ('-')*[0-9];

COMP        : ('<='|'>'|'<'|'>='|'=');
NEG         : '!';
BOOL_OP     : ('&'|'|'|'->');
