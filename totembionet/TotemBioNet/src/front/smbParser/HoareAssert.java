package front.smbParser;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

enum HoareAssertType {
    NEG, BOOL_OP, COMP, SIMPLE_TERM_NUM, SIMPLE_TERM_ID, SIMPLE_TERM_KID, OPER_TERM;
}

public class HoareAssert {

    private HoareAssertType type;
    private String op;
    private HoareAssert expr1;
    private HoareAssert expr2;
    private String term;

    public HoareAssert () {
        this.type = null;
        this.op = null;
        this.expr1 = null;
        this.expr2 = null;
        this.term = null;
    }

    public HoareAssertType getType() {
        return type;
    }

    public String getOp() {
        return op;
    }

    public HoareAssert getExpr1() {
        return expr1;
    }

    public HoareAssert getExpr2() {
        return expr2;
    }

    public String getTerm() {
        return term;
    }


    public void setType(HoareAssertType type) {
        this.type = type;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public void setExpr1(HoareAssert expr1) {
        this.expr1 = expr1;
    }

    public void setExpr2(HoareAssert expr2) {
        this.expr2 = expr2;
    }

    public void setTerm(String term) {
        this.term = term;
    }


}