package front.smbParser;

import back.jlogic.Formula;
import back.net.Reg;

import java.util.*;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class Regul {
    private String id;
    private List<String> targets;
    private String formulaString;
    private RegOp formulaTree;

    public Regul(String id, List<String> targets, String formulaString, RegOp formulaTree) {
        this.id = id;
        this.targets = targets;
        this.formulaString = formulaString;
        this.formulaTree = formulaTree;
    }

    public String getId() {
        return id;
    }

    public List<String> getTargets() {
        return targets;
    }

    public String getFormulaString() {
        return formulaString;
    }

    public RegOp getFormulaTree() {
        return formulaTree;
    }

    public Reg toBack() throws Exception{
        Formula formula = this.formulaTree.toFormula();
        return new Reg(this.id, formula);
    }

    public void addTarget(String target){
        this.targets.add(target);
    }

}