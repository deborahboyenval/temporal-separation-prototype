package front.smbParser;

import java.util.List;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class Param {

    private String gene;
    private String SMBid;
    private String finalId;
    private List<Regul> regulations;
    private Integer valMin;
    private Integer valMax;

    public Param(String gene, String SMBid, String finalId,List<Regul> regulations, Integer valMin, Integer valMax) {
        this.gene = gene;
        this.SMBid = SMBid;
        this.finalId =finalId ;
        this.regulations = regulations;
        this.valMin = valMin;
        this.valMax = valMax;
    }

    public String getGene() {
        return gene;
    }

    public String getSMBid() {
        return SMBid;
    }
    public String getFinalid() {
        return finalId;
    }

    public Integer getValMin() {
        return valMin;
    }

    public Integer getValMax() {
        return valMax;
    }

    public List<Regul> getRegulations() {
        return regulations;
    }

//    public Para toBack() throws Exception{
//        OrderedArrayList<Reg> regs = new OrderedArrayList<>();
//        for(Regul reg : this.regulations){
//                regs.add(reg.toBack());
//        }
//        return new Para(gene, regs);
//    }
}