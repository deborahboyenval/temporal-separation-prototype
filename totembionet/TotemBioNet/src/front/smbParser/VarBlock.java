package front.smbParser;

import back.net.*;
import back.jlogic.blogic.*;
import back.jlogic.CTLlogic.*;
import util.TotemBionetException;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class VarBlock {

    private List<Var> data;
    private HashSet<String> isInPreHoare;

    public VarBlock() {
        this.data = new ArrayList<>();
        this.isInPreHoare = new HashSet<>();
    }

    public List<Var> getData() {
        return data;
    }

    public boolean existsVar(String varId, VarType type){
        if(type != null){
            return data.stream().filter(var -> var.getId().equals(varId) && var.getVarType() == type).count()>0;
        }else {
            return data.stream().filter(var -> var.getId().equals(varId)).count()>0;
        }
    }

    public void addVar(VarType type, String id, Integer valMin, Integer valMax , boolean snoussi) {
        this.data.add(new Var(type, id, valMin, valMax,snoussi));
    }

    public Var getVarWithId(String id){
        List<Var> varList = new ArrayList<>(this.data.stream().filter(var -> var.getId().equals(id)).collect(Collectors.toList()));
        if(varList.size()==1){
            return varList.get(0);
        } else {
            System.err.println(varList.size() + " var with this id.");
            return null;
        }
    }

    public String toJSON(){
        long env_varCount = data.stream().filter(var -> var.getVarType() == VarType.ENV_VAR).count();
        long varCount =  data.stream().filter(var -> var.getVarType() == VarType.VAR).count();
        String env_varBlock = "{\"blocktype\" : \"env_var\""
                             +", \"value\" : {\"count\" : "+ env_varCount
                                           +" , \"data\" : [";

        String varBlock = ",{\"blocktype\": \"var\""
                        + " , \"value\" : {\"count\" : "+ varCount
                                  +" , \"data\" : [";

        for(Var var : this.data){
            if(var.getVarType() == VarType.ENV_VAR){
                env_varBlock += "{ \"id\" : \"" + var.getId() + "\", \"val\" : " + var.getMin() + "},";
            }else {
                varBlock += "{ \"id\" : \"" + var.getId() + "\", \"min\" : " + var.getMin() + ", \"max\" : " + var.getMax() + "},";
            }
        }
        if(env_varCount > 0)
            env_varBlock = env_varBlock.substring(0, env_varBlock.length() - 1);
        env_varBlock += "]}}";

        if(varCount > 0)
            varBlock = varBlock.substring(0, varBlock.length() - 1);
        varBlock += "]}}";
        return env_varBlock + varBlock;
    }

    public List<Gene> getBackFormat(){
        List<Gene> geneList = new ArrayList<>();
        for(Var var : this.data){
            geneList.add(var.toBack());
        }
        return geneList;
    }

    public void addPreVar(String id){
        isInPreHoare.add(id);
    }

    public boolean allVarInPreHoare() {
        for (Var v:data) {
            if ((v.getVarType() != VarType.ENV_VAR) &&!isInPreHoare.contains(v.getId()))
                return false;
        }
        return true;
    }
}