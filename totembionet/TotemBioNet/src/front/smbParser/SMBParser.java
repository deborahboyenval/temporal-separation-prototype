package front.smbParser;

import front.smbParser.grammar.RRGLexer;
import front.smbParser.grammar.RRGParser;
import org.antlr.v4.runtime.*;
import util.TotemBionetException;

import java.io.*;

/**
 *
 * Classe de lancement du parsing
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class SMBParser {

    private String inputFileName;
    private Visitor visitor;
    private int verbose;

    public SMBParser(String path,String fileName,int verbose) {
        this.inputFileName = path+fileName+".smb";
        this.visitor = new Visitor();
        this.verbose=verbose;
    }

    public Visitor parse() throws Exception{
        long before = System.currentTimeMillis();

        CharStream inputStream = null;
        try {
            inputStream = CharStreams.fromFileName(inputFileName);
        } catch(IOException io){
            throw new TotemBionetException("NoSuchFile: " + inputFileName);
        }

        RRGLexer lexer = new RRGLexer(inputStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        RRGParser parser = new RRGParser(commonTokenStream);
        RRGParser.ProgContext progContext = parser.prog();

        int syntaxErrorsCount = parser.getNumberOfSyntaxErrors();
        if(syntaxErrorsCount>0){
            throw new Exception(" *** Failure - " + syntaxErrorsCount +" syntax errors *** ");
        }else {
            visitor = new Visitor();
            visitor.visit(progContext);
            int semanticErrorsCount = visitor.getErrorCounter();
            if (semanticErrorsCount > 0) {
                //Throws exception
                throw new Exception(" *** Failure - " + semanticErrorsCount + " semantic errors *** ");
            }
        }
        if (verbose>1){
            long between = System.currentTimeMillis();
            System.out.println("*****************");
            System.out.println("Time for parsing SMB file  " + (between - before) + "ms");
            System.out.println("*****************\n");
        }
        return visitor;
    }

}