// Generated from TotemBioNet/src/front/smbParser/grammar/RRG.g4 by ANTLR 4.7.1
package front.smbParser.grammar;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RRGLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		KENV_VAR=25, KVAR=26, KREG=27, KPARA=28, KCTL=29, KHOARE=30, KPRE=31, 
		KTRACE=32, KPOST=33, CTL_PREFIX_1=34, CTL_PREFIX_2=35, KID=36, ID=37, 
		NUM=38, NS=39, IMPL=40, CIBLE=41, COMP=42, EQ=43, SEUIL=44, SEMI=45, NEG=46, 
		BOOL_OP=47, OPER=48, WS=49, COMMENT=50;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "KENV_VAR", 
		"KVAR", "KREG", "KPARA", "KCTL", "KHOARE", "KPRE", "KTRACE", "KPOST", 
		"CTL_PREFIX_1", "CTL_PREFIX_2", "KID", "ID", "NUM", "NS", "IMPL", "CIBLE", 
		"COMP", "EQ", "SEUIL", "SEMI", "NEG", "BOOL_OP", "OPER", "WS", "COMMENT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'END'", "'..'", "'['", "']'", "'('", "')'", "'U'", "':'", "'{'", 
		"','", "'}'", "'Skip'", "':='", "'+'", "'-'", "'If'", "'Then'", "'Else'", 
		"'While'", "'With'", "'Do'", "'Forall'", "'Exists'", "'Assert'", "'ENV_VAR'", 
		"'VAR'", "'REG'", "'PARA'", null, "'HOARE'", "'PRE'", "'TRACE'", "'POST'", 
		null, null, null, null, null, "'(NS)'", "'->'", "'=>'", null, "'='", "'>='", 
		"';'", "'!'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, "KENV_VAR", "KVAR", "KREG", "KPARA", "KCTL", "KHOARE", "KPRE", "KTRACE", 
		"KPOST", "CTL_PREFIX_1", "CTL_PREFIX_2", "KID", "ID", "NUM", "NS", "IMPL", 
		"CIBLE", "COMP", "EQ", "SEUIL", "SEMI", "NEG", "BOOL_OP", "OPER", "WS", 
		"COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public RRGLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "RRG.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\64\u014f\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\3\2"+
		"\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3"+
		"\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3"+
		"\17\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3"+
		"\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\26\3"+
		"\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3"+
		"\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3"+
		"\32\3\32\3\32\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3"+
		"\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u00dc"+
		"\n\36\3\37\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3!\3!\3!\3!\3!\3!\3\""+
		"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\5#\u00ff\n#\3$\3"+
		"$\3%\3%\3%\3%\3%\7%\u0108\n%\f%\16%\u010b\13%\3%\3%\3%\7%\u0110\n%\f%"+
		"\16%\u0113\13%\7%\u0115\n%\f%\16%\u0118\13%\3&\3&\7&\u011c\n&\f&\16&\u011f"+
		"\13&\3\'\3\'\3(\3(\3(\3(\3(\3)\3)\3)\3*\3*\3*\3+\3+\3+\5+\u0131\n+\3,"+
		"\3,\3-\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\6\62\u0141\n\62\r\62"+
		"\16\62\u0142\3\62\3\62\3\63\3\63\7\63\u0149\n\63\f\63\16\63\u014c\13\63"+
		"\3\63\3\63\2\2\64\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31"+
		"\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65"+
		"\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64"+
		"\3\2\13\4\2CCGG\4\2C\\c|\6\2\62;C\\aac|\3\2\62;\4\2>>@@\4\2((~~\4\2--"+
		"//\5\2\13\f\17\17\"\"\3\2\f\f\2\u015b\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2"+
		"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2"+
		"\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3"+
		"\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3"+
		"\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65"+
		"\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3"+
		"\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2"+
		"\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2"+
		"[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\3g\3"+
		"\2\2\2\5k\3\2\2\2\7n\3\2\2\2\tp\3\2\2\2\13r\3\2\2\2\rt\3\2\2\2\17v\3\2"+
		"\2\2\21x\3\2\2\2\23z\3\2\2\2\25|\3\2\2\2\27~\3\2\2\2\31\u0080\3\2\2\2"+
		"\33\u0085\3\2\2\2\35\u0088\3\2\2\2\37\u008a\3\2\2\2!\u008c\3\2\2\2#\u008f"+
		"\3\2\2\2%\u0094\3\2\2\2\'\u0099\3\2\2\2)\u009f\3\2\2\2+\u00a4\3\2\2\2"+
		"-\u00a7\3\2\2\2/\u00ae\3\2\2\2\61\u00b5\3\2\2\2\63\u00bc\3\2\2\2\65\u00c4"+
		"\3\2\2\2\67\u00c8\3\2\2\29\u00cc\3\2\2\2;\u00db\3\2\2\2=\u00dd\3\2\2\2"+
		"?\u00e3\3\2\2\2A\u00e7\3\2\2\2C\u00ed\3\2\2\2E\u00fe\3\2\2\2G\u0100\3"+
		"\2\2\2I\u0102\3\2\2\2K\u0119\3\2\2\2M\u0120\3\2\2\2O\u0122\3\2\2\2Q\u0127"+
		"\3\2\2\2S\u012a\3\2\2\2U\u0130\3\2\2\2W\u0132\3\2\2\2Y\u0134\3\2\2\2["+
		"\u0137\3\2\2\2]\u0139\3\2\2\2_\u013b\3\2\2\2a\u013d\3\2\2\2c\u0140\3\2"+
		"\2\2e\u0146\3\2\2\2gh\7G\2\2hi\7P\2\2ij\7F\2\2j\4\3\2\2\2kl\7\60\2\2l"+
		"m\7\60\2\2m\6\3\2\2\2no\7]\2\2o\b\3\2\2\2pq\7_\2\2q\n\3\2\2\2rs\7*\2\2"+
		"s\f\3\2\2\2tu\7+\2\2u\16\3\2\2\2vw\7W\2\2w\20\3\2\2\2xy\7<\2\2y\22\3\2"+
		"\2\2z{\7}\2\2{\24\3\2\2\2|}\7.\2\2}\26\3\2\2\2~\177\7\177\2\2\177\30\3"+
		"\2\2\2\u0080\u0081\7U\2\2\u0081\u0082\7m\2\2\u0082\u0083\7k\2\2\u0083"+
		"\u0084\7r\2\2\u0084\32\3\2\2\2\u0085\u0086\7<\2\2\u0086\u0087\7?\2\2\u0087"+
		"\34\3\2\2\2\u0088\u0089\7-\2\2\u0089\36\3\2\2\2\u008a\u008b\7/\2\2\u008b"+
		" \3\2\2\2\u008c\u008d\7K\2\2\u008d\u008e\7h\2\2\u008e\"\3\2\2\2\u008f"+
		"\u0090\7V\2\2\u0090\u0091\7j\2\2\u0091\u0092\7g\2\2\u0092\u0093\7p\2\2"+
		"\u0093$\3\2\2\2\u0094\u0095\7G\2\2\u0095\u0096\7n\2\2\u0096\u0097\7u\2"+
		"\2\u0097\u0098\7g\2\2\u0098&\3\2\2\2\u0099\u009a\7Y\2\2\u009a\u009b\7"+
		"j\2\2\u009b\u009c\7k\2\2\u009c\u009d\7n\2\2\u009d\u009e\7g\2\2\u009e("+
		"\3\2\2\2\u009f\u00a0\7Y\2\2\u00a0\u00a1\7k\2\2\u00a1\u00a2\7v\2\2\u00a2"+
		"\u00a3\7j\2\2\u00a3*\3\2\2\2\u00a4\u00a5\7F\2\2\u00a5\u00a6\7q\2\2\u00a6"+
		",\3\2\2\2\u00a7\u00a8\7H\2\2\u00a8\u00a9\7q\2\2\u00a9\u00aa\7t\2\2\u00aa"+
		"\u00ab\7c\2\2\u00ab\u00ac\7n\2\2\u00ac\u00ad\7n\2\2\u00ad.\3\2\2\2\u00ae"+
		"\u00af\7G\2\2\u00af\u00b0\7z\2\2\u00b0\u00b1\7k\2\2\u00b1\u00b2\7u\2\2"+
		"\u00b2\u00b3\7v\2\2\u00b3\u00b4\7u\2\2\u00b4\60\3\2\2\2\u00b5\u00b6\7"+
		"C\2\2\u00b6\u00b7\7u\2\2\u00b7\u00b8\7u\2\2\u00b8\u00b9\7g\2\2\u00b9\u00ba"+
		"\7t\2\2\u00ba\u00bb\7v\2\2\u00bb\62\3\2\2\2\u00bc\u00bd\7G\2\2\u00bd\u00be"+
		"\7P\2\2\u00be\u00bf\7X\2\2\u00bf\u00c0\7a\2\2\u00c0\u00c1\7X\2\2\u00c1"+
		"\u00c2\7C\2\2\u00c2\u00c3\7T\2\2\u00c3\64\3\2\2\2\u00c4\u00c5\7X\2\2\u00c5"+
		"\u00c6\7C\2\2\u00c6\u00c7\7T\2\2\u00c7\66\3\2\2\2\u00c8\u00c9\7T\2\2\u00c9"+
		"\u00ca\7G\2\2\u00ca\u00cb\7I\2\2\u00cb8\3\2\2\2\u00cc\u00cd\7R\2\2\u00cd"+
		"\u00ce\7C\2\2\u00ce\u00cf\7T\2\2\u00cf\u00d0\7C\2\2\u00d0:\3\2\2\2\u00d1"+
		"\u00d2\7E\2\2\u00d2\u00d3\7V\2\2\u00d3\u00dc\7N\2\2\u00d4\u00d5\7H\2\2"+
		"\u00d5\u00d6\7C\2\2\u00d6\u00d7\7K\2\2\u00d7\u00d8\7T\2\2\u00d8\u00d9"+
		"\7E\2\2\u00d9\u00da\7V\2\2\u00da\u00dc\7N\2\2\u00db\u00d1\3\2\2\2\u00db"+
		"\u00d4\3\2\2\2\u00dc<\3\2\2\2\u00dd\u00de\7J\2\2\u00de\u00df\7Q\2\2\u00df"+
		"\u00e0\7C\2\2\u00e0\u00e1\7T\2\2\u00e1\u00e2\7G\2\2\u00e2>\3\2\2\2\u00e3"+
		"\u00e4\7R\2\2\u00e4\u00e5\7T\2\2\u00e5\u00e6\7G\2\2\u00e6@\3\2\2\2\u00e7"+
		"\u00e8\7V\2\2\u00e8\u00e9\7T\2\2\u00e9\u00ea\7C\2\2\u00ea\u00eb\7E\2\2"+
		"\u00eb\u00ec\7G\2\2\u00ecB\3\2\2\2\u00ed\u00ee\7R\2\2\u00ee\u00ef\7Q\2"+
		"\2\u00ef\u00f0\7U\2\2\u00f0\u00f1\7V\2\2\u00f1D\3\2\2\2\u00f2\u00f3\7"+
		"G\2\2\u00f3\u00ff\7Z\2\2\u00f4\u00f5\7C\2\2\u00f5\u00ff\7Z\2\2\u00f6\u00f7"+
		"\7G\2\2\u00f7\u00ff\7H\2\2\u00f8\u00f9\7C\2\2\u00f9\u00ff\7H\2\2\u00fa"+
		"\u00fb\7G\2\2\u00fb\u00ff\7I\2\2\u00fc\u00fd\7C\2\2\u00fd\u00ff\7I\2\2"+
		"\u00fe\u00f2\3\2\2\2\u00fe\u00f4\3\2\2\2\u00fe\u00f6\3\2\2\2\u00fe\u00f8"+
		"\3\2\2\2\u00fe\u00fa\3\2\2\2\u00fe\u00fc\3\2\2\2\u00ffF\3\2\2\2\u0100"+
		"\u0101\t\2\2\2\u0101H\3\2\2\2\u0102\u0103\7M\2\2\u0103\u0104\7a\2\2\u0104"+
		"\u0105\3\2\2\2\u0105\u0109\t\3\2\2\u0106\u0108\t\4\2\2\u0107\u0106\3\2"+
		"\2\2\u0108\u010b\3\2\2\2\u0109\u0107\3\2\2\2\u0109\u010a\3\2\2\2\u010a"+
		"\u0116\3\2\2\2\u010b\u0109\3\2\2\2\u010c\u010d\7<\2\2\u010d\u0111\t\3"+
		"\2\2\u010e\u0110\t\4\2\2\u010f\u010e\3\2\2\2\u0110\u0113\3\2\2\2\u0111"+
		"\u010f\3\2\2\2\u0111\u0112\3\2\2\2\u0112\u0115\3\2\2\2\u0113\u0111\3\2"+
		"\2\2\u0114\u010c\3\2\2\2\u0115\u0118\3\2\2\2\u0116\u0114\3\2\2\2\u0116"+
		"\u0117\3\2\2\2\u0117J\3\2\2\2\u0118\u0116\3\2\2\2\u0119\u011d\t\3\2\2"+
		"\u011a\u011c\t\4\2\2\u011b\u011a\3\2\2\2\u011c\u011f\3\2\2\2\u011d\u011b"+
		"\3\2\2\2\u011d\u011e\3\2\2\2\u011eL\3\2\2\2\u011f\u011d\3\2\2\2\u0120"+
		"\u0121\t\5\2\2\u0121N\3\2\2\2\u0122\u0123\7*\2\2\u0123\u0124\7P\2\2\u0124"+
		"\u0125\7U\2\2\u0125\u0126\7+\2\2\u0126P\3\2\2\2\u0127\u0128\7/\2\2\u0128"+
		"\u0129\7@\2\2\u0129R\3\2\2\2\u012a\u012b\7?\2\2\u012b\u012c\7@\2\2\u012c"+
		"T\3\2\2\2\u012d\u012e\7>\2\2\u012e\u0131\7?\2\2\u012f\u0131\t\6\2\2\u0130"+
		"\u012d\3\2\2\2\u0130\u012f\3\2\2\2\u0131V\3\2\2\2\u0132\u0133\7?\2\2\u0133"+
		"X\3\2\2\2\u0134\u0135\7@\2\2\u0135\u0136\7?\2\2\u0136Z\3\2\2\2\u0137\u0138"+
		"\7=\2\2\u0138\\\3\2\2\2\u0139\u013a\7#\2\2\u013a^\3\2\2\2\u013b\u013c"+
		"\t\7\2\2\u013c`\3\2\2\2\u013d\u013e\t\b\2\2\u013eb\3\2\2\2\u013f\u0141"+
		"\t\t\2\2\u0140\u013f\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0140\3\2\2\2\u0142"+
		"\u0143\3\2\2\2\u0143\u0144\3\2\2\2\u0144\u0145\b\62\2\2\u0145d\3\2\2\2"+
		"\u0146\u014a\7%\2\2\u0147\u0149\n\n\2\2\u0148\u0147\3\2\2\2\u0149\u014c"+
		"\3\2\2\2\u014a\u0148\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014d\3\2\2\2\u014c"+
		"\u014a\3\2\2\2\u014d\u014e\b\63\2\2\u014ef\3\2\2\2\f\2\u00db\u00fe\u0109"+
		"\u0111\u0116\u011d\u0130\u0142\u014a\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}