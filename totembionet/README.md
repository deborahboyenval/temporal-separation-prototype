# TotemBioNet

## Project presentation

TotemBioNet is a tool dedicated to parameter identification in Biological Regulatory Networks (BRN). Given a (BRN) and some properties on the dynamics of the system, TotemBioNet outputs all the models that satisfy these properties. 

Properties on the dynamics of the system may be :
* values of known parameters
* CTL properties
* FAIRCTL properties (a CTL property on infinite fair paths p, such that if a vertex v occurs infinitely often in p, then all the edges starting from v occur infinitely often in p)
* a Hoare trace: a triple {P} trace {Q} where P is an initial operable state, trace is an observation trace and Q is a final obervable state.

See documentation in directory `./doc`.
        
## Installation

1. **Clone or download the project**

    `git clone https://gitlab.com/totembionet/totembionet.git`

2. **Install dependencies**

    This project is written in Java and Ocaml. The input file is parsed using antlr4, the CTL properties are checked with NuSMV.

    - Ocaml: at least version 4.03.0 is required  see https://ocaml.org/docs/install.html
    - NuSMV: a zipped compatible version is provided in `./lib/NuSMV`. You need to unzip it.
    - antlr4: jar files for antlr4 are provided in `./lib/antlr4`

    TotemBioNet also provides a parser from .graphml to internal
    format. You can download the yEd graphical tool here :
    https://www.yworks.com/products/yed/download

3. **Compile the project**

    To compile the project, just enter `make` in this directory. Two scripts will be created
* `totembionet` which should be used to run TotemBioNet 
* `ASTtreeViewer` to visualize the AST (Abstract Syntax Tree) of the .smb file

    You can test the program with 
* `./totembionet examples/mucusOperon/mucusOperon.smb` to run TotemBioNet
* `./ASTTreeViewer examples/mucusOperon/mucusOperon.smb` to view the AST of `mucusOperon.smb` 

    Other examples are provided in the `examples` directory.

4. **Install the project**

    Type `make install` (eventually preceded by a `sudo`) to install the project in the install directory.  The install directory is defined at the top of the `Makefile` in the `INSTALL_DIR` variable (defaults to `/usr/local/TotemBioNet`).
    
    Once installed, eventually update your `PATH` variable with something like `PATH=$PATH:<TotemBioNet install directory>` to give access to the command `totembionet` everywhere. 
    

## Short user manual

A more complete user manual can be found in the doc directory. 

### totembionet

* input: a .smb file containing the BRN and the properties on the dynamics of the system. A .smb file is composed of several blocs :
    - ENV_VAR (optional): defines environment variables. This variables are fixed, and correspond to an experimental state. 
    - VAR: variables of the system
    - REG: set of regulations of the system. Each regulation is of the form :
            reg_name multiplex -> target_list
where multiplex is a logical formula on sources of the regulation.
    - PARA (optional): use to assign values to known parameters.
    - HOARE: a Hoare triple with:
        PRE: {set of equalities of the form var=value}
        TRACE: a trace
        POST: {set of equalities of the form var=value}
    - CTL (optional): a CTL property on variables
    - FAIRCTL (optional): a fair CTL property on variables

The complete grammar can be found in file TotemBioNet/src/front/smbParser/grammar/RRG.g4)

* output: a `.out` file which contains all the parameters labeled OK or KO depending on which parameter respects the dynamic properties of the system.

* options:
    - o name: Name of the output file for parameter identification
    - csv: Generates also the output of model identification in a CSV file 
    - verbose n : verbosity level. 1: more information is printed, 2: auxilliary files (for NuSMV and Hoare) are not deleted");
    - paras: Print the set of all effective parameters
    - json: Translate the .smb file in a JSON format
    - simu : To start simulation mode (without Hoare, without environment variables, alpha version)
    - yed : Translate <input>.graphml into <input>FromYed.smb. <input>.graphml contains BRN information built from yEd using some restrictive conventions (see user manual)
    - help : Help guide.

### ASTTreeViewer

* input : a .smb file 
* output : display the AST tree (i.e. antlr4 tree)

### Makefile

* `make`: to recompile java files

* `make updateSMBGrammar`: update the SMB grammar (RRG.g4) and re-generate the java visitors

* `make updateHoareGrammar`: update the Hoare grammar and re-generate the java visitors

## Contributors
* Adrien Richard, I3S, who wrote SMBioNet. TotemBioNet is a new version of SMBionet. It uses the parameter enumeration process of SMBioNet. The generation of NuSMV automatom is also very close to the one of SMBioNet.
* Maxime Folschette, Centrale Lille, who wrote the Ocaml code which computes the weakest precondition of a Hoare triple
* Sokhna N'deye, Master student a Polytech'Nice Sophia, who wrote the antlr4 parser. She did a very good job !
* Erick Gallésio, Polytech'Nice Sophia, who was co-advisor of the master internship of Sokhna
* Hélène Collavizza, Polytech'Nice Sophia, Helene.COLLAVIZZA@univ-cotedazur.fr, project manager.

## Bibliography

See the doc directory for a more complete user manual and a note on FAIR CTL.
The following articles can be found from the web page of Jean-Paul Comet :

* G. Bernot; J.-P. Comet; Z. Khalis; A. Richard; O.F. Roux,. A Genetically Modified Hoare Logic. Theoretical Computer Science. 2018.

* G. Bernot, J.-P. Comet and E.H. Snoussi. Formal methods applied to gene network modelling. In Logical Modeling of Biological Systems (eds. K. Inoue and L. Fariñas). pp. 245-289, ISBN 978-1-84821-680-8, ISTE & Wiley, 2014. 
 
* Z. Khalis, J.-P. Comet, A. Richard, and G. Bernot. The SMBioNet Method for Discovering Models of Gene Regulatory Networks. Genes, Genomes and Genomics. 3(special issue 1):15-22, 2009. 



