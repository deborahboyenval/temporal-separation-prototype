# Temporal separation prototype

## Project presentation
The temporal separation prototype is an implementation in Prolog of a predicat formalizing one of the fundamental property of a biological checkpoint. It has been defined within the PhD manuscript of Déborah Boyenval (page 133, definition 31). 

## Installation of the prototype

1. Dependencies 
- Csvtool : `sudo apt-get install csvtool`
- Swiprolog version 7.6.4

2. Some dependencies of TotemBioNet as well : 
- Ocaml: at least version 4.03.0 is required  see https://ocaml.org/docs/install.html
- NuSMV: a zipped compatible version is provided in `./lib/NuSMV`. You need to unzip it.
- antlr4: jar files for antlr4 are provided in `./lib/antlr4`

3. Clone the project with `git clone https://gitlab.com/deborahboyenval/temporal-separation-prototype.git`

## Project status
At the stage of the very first implementation sufficient to at least reproduce the results of the proof of concept of the PhD.
We also provide a repository that stores the results of the proof of concept performed during the thesis. It includes an application of:
- the temporal separation predicate for G1/S, S/G2 and G2/M using a canonical definition of G1, S, G2 and M;
- the irreversibility predicate which consists in checking three formulas in fair CTL;
- and the first identification of the parameterizations of our biological regulatory graph with multiplexes, which is essential to realize a computationally efficient proof of concept. 

## Short user manual

1. The **user input** is a set of René Thomas' models. The reader is referred to page 30 of the thesis manuscript for a formal definition of a model. A set of René Thomas' models is encoded as a TotemBioNet input file with the extension `.smb`. This file specifies a biological regulatory graph with multiplexes and the range of values (reduced or not using biological knowledge) that each of its dynamic parameters can take. Please follow the link for more information about the syntax of a TotemBioNet input file: https://gitlab.com/totembionet/totembionet.
The name of the input file must be filled in as the value of the "output_yED" field in the `user_inputs.txt` file.
2. Use the command `swpl` in a terminal
3. Within the swiprolog console, write `consult("temporal_separation.pl").`
4. The input command is of the form: `temporal_separation(E1,E2,S1,S2,S3,Res)` where:
	- `E1` is the list of events necessary for the realization of a phase pi_i.
	- `E2` is the list of events necessary for the realization of the next phase pi_i+1.
	- `S1` is the initial state of pi_i.
	- `S2` is the final state of pi_i and the initial state of pi_i+1. It is also name the checkpoint state pi_i / pi_i+1.
	- `S3` is the final state of pi_i+1.
	- `Res` encodes a solution of the predicate (cf. section Outputs below)
5. `findall([X,Y],temporal_separation(...))` allows us to identify all the solutions of the predicate.

Let's start the tests with the usecase of the PhD (page 140 of the manuscript): 
- Test G1/S: `time(findall([X,Y],temporal_separation(['sk+','en-','sk+'],['a+','sk-','sk-'],"sk=0,ep=0,a=0,b=0,en=1,gf=1","sk=2,ep=0,a=0,b=0,en=0,gf=1","sk=0,ep=0,a=1,b=0,en=0,gf=1",X,Y),Res)).`
- Test S/G2: `time((findall([X,Y],temporal_separation(['a+','sk-','sk-'],['a+','b+'],"sk=2,ep=0,a=0,b=0,en=0,gf=1","sk=0,ep=0,a=1,b=0,en=0,gf=1","sk=0,ep=0,a=2,b=1,en=0,gf=1",X,Y),Res))).`
- Test G2/M: `time((findall([X,Y],temporal_separation(['a+','b+'],['b+','ep+','a-','a-','b-','b-','en+','ep-'],"sk=0,ep=0,a=1,b=0,en=0,gf=1","sk=0,ep=0,a=2,b=1,en=0,gf=1","sk=0,ep=0,a=0,b=0,en=1,gf=1",X,Y),Res))).`

## Outputs

1. Directly Within the Swiprolog console. 
`Res` is the free variable that specifies the solution events of the predicate. 
`Res` is a list of tuples of the form `[[e1,e2], [e3,e4]]`, where each tuple is composed of two non-permutable events in the sense that element `e1` is required before element `e2`, and `e3` is also required before `e4`. 
Referring to the arguments of the `temporal_separation` predicate, `e1` and `e3` belong to a given phase and `e2` and `e4` belong to the next phase.  

2. Output files
The `OutputFiles` directory is generated automatically when the predicate is executed. It contains two files:
- `estRequisNS.out`: stores on each line the feasible sequences of the current phase and its next phase from the `S1` state, the associated constraint estRequis and a boolean that specifies whether the constraint is satisfiable. 
- `estRequis.out`: identifies the values of the parameters involved in the isRequired constraint that make it satisfiable. 

## Contributors
Déborah Boyenval (I3S laboratory) is the main contributor. 
A specific version of the source code of totembionet is installed at the same time as the prototype . All rights reserved to Hélène Collavizza about totembionet source files (gitlab.com/totembionet/totembionet). 

## Bug Reports
Everything developed here has been tested on Ubuntu 20.04.4 LTS. Feel free to send any bug report to: boyenval@i3s.unice.fr.
Tests on MacOS are expected shortly. 
