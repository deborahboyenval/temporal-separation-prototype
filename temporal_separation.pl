% Constant provided by the user :  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%etaI("sk=2,ep=0,a=0,b=0,en=0,gf=1"). %etaG1/S
%etaF("sk=0,ep=0,a=1,b=0,en=0,gf=1"). %etaS/G2
%Observed phase: ['a+','sk-','sk-'].  %S


% Predicates :
%%%%%%%%%%%%%%

first([E|_],E).

permNoDup(L,P):-findall(Dp,permutation(L,Dp),DLp),
				sort(DLp,Lp),
				member(P,Lp). 
				
evalConstraint(Constraint):-(Constraint=="true").
				
% a version of the Prolog permutation predicate that ignore the duplicates

computeWP(Perm,Eta0,EtaN):-atomic_list_concat(Perm,';',Path),
			atomic_list_concat(["./computeWP.sh ","'",Eta0,"' '",Path,"' '",EtaN,"'"],InputShell), %InputShell is an atom
			atom_string(InputShell,Cmd), %Cmd is a string
			shell(Cmd).
			
% Create and execute a shell command. Perm is a list of strings of the form :  ["E1",...,"En"] and is converted into an atom named Path of the form : "E1;...;En". Cmd is of the form : "./computeWP.sh Eta0 Path EtaN" where Eta0, Path and EtaN are susbtituted by their value (string). 

existModel(Model):-not(Model==end_of_file),not(Model=="0").

% Model == end_of_file : wp unsatisfiable
% Modem == 0 : wp not satisfied
% Model >0 : wp satisfied 

getContent(File,Model):- open(File, read, STREAM),
		read_line_to_string(STREAM,Model),
		%write(Model), %DEBUG
		close(STREAM).
		
% Read and store within the variable Model the content of the File given as input.

wpSatisfied(Perm,EtaI,EtaF):-computeWP(Perm,EtaI,EtaF),
		getContent("modelNumber.txt",Num),
		existModel(Num).
		
% modelNumber.txt store the number of selected model identified by TotemBioNet.

%Main Predicates 
%%%%%%%%%%%%%%%%

peutInitier(Phase,Eta0I,EtaNI,Perm,E):-permNoDup(Phase,Perm), 
			first(Perm,E),
			wpSatisfied(Perm,Eta0I,EtaNI).

peutClore(Phase,Eta0C,EtaNC,Perm,E):-permNoDup(Phase,Perm),
			last(Perm,E),
			wpSatisfied(Perm,Eta0C,EtaNC).

estRequis(X,Y,EtaNC,PermC,PermI):-atomic_list_concat(PermC,';',PathC),
				atomic_list_concat(PermI,';',PathI),
				atomic_list_concat(["./estRequis.sh ","'",EtaNC,"' '",X,"' '",Y,"' '",PathC,"' '",PathI,"'"],Input),
				atom_string(Input,Cmd),
				%writeln(Cmd), %DEBUG
				shell(Cmd),
				getContent("estRequis.txt",Constraint),
				evalConstraint(Constraint).

temporal_separation(PhaseC,PhaseI,Eta0C,EtaNC,EtaNI,X,Y):-shell("./init_estRequisOut.sh"),!,
				peutClore(PhaseC,Eta0C,EtaNC,PermC,X),
				peutInitier(PhaseI,EtaNC,EtaNI,PermI,Y),
				%write(PermC),write(PermI), %DEBUG
				%write(X),write(Y), %DEBUG
				estRequis(X,Y,EtaNC,PermC,PermI).

%Tests : 
%%%%%%%%

% Open an unix terminal
% Write command "swipl"
% Write "consult("temporal_separation.pl")."
% Let's start the tests with the usecase of the PhD


%% G1/S %%
% time(findall([X,Y],temporal_separation(['sk+','en-','sk+'],['a+','sk-','sk-'],"sk=0,ep=0,a=0,b=0,en=1,gf=1","sk=2,ep=0,a=0,b=0,en=0,gf=1","sk=0,ep=0,a=1,b=0,en=0,gf=1",X,Y),Res)).

%% S/G2 %%
% time((findall([X,Y],temporal_separation(['a+','sk-','sk-'],['a+','b+'],"sk=2,ep=0,a=0,b=0,en=0,gf=1","sk=0,ep=0,a=1,b=0,en=0,gf=1","sk=0,ep=0,a=2,b=1,en=0,gf=1",X,Y),Res))).

%%% G2/M %%%
%time((findall([X,Y],temporal_separation(['a+','b+'],['b+','ep+','a-','a-','b-','b-','en+','ep-'],"sk=0,ep=0,a=1,b=0,en=0,gf=1","sk=0,ep=0,a=2,b=1,en=0,gf=1","sk=0,ep=0,a=0,b=0,en=1,gf=1",X,Y),Res))).

%%% Exploration SAC %%%
%initial state of M : sk=0,ep=0,a=2,b=1,en=0,gf=1
%final -------------: sk=0,ep=0,a=0,b=0,en=1,gf=1

%if b+;ep+;a- as early M then 
%final of early M is: sk=0,ep=1,a=1,b=2,en=0,gf=1
%time((findall([X,Y],temporal_separation(['b+','ep+','a-'],['b-','a-','b-','ep-','en+'],"sk=0,ep=0,a=2,b=1,en=0,gf=1","sk=0,ep=1,a=1,b=2,en=0,gf=1","sk=0,ep=0,a=0,b=0,en=1,gf=1",X,Y),Res))).

%if b+;ep+;a-;a- as early M then 
%final state of early M is: sk=0,ep=1,a=0,b=2,en=0,gf=1
%time((findall([X,Y],temporal_separation(['b+','ep+','a-','a-'],['b-','b-','ep-','en+'],"sk=0,ep=0,a=2,b=1,en=0,gf=1","sk=0,ep=1,a=0,b=2,en=0,gf=1","sk=0,ep=0,a=0,b=0,en=1,gf=1",X,Y),Res))).
