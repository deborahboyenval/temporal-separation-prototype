#!/usr/bin/env bash

#Users input
output_yED=$(cat user_inputs.txt | cut -d "=" -f2) 
pathTotem="./totembionet/totembionet" 
pathTotemFiles="./TotemFiles"

#Smb input file creation
cat ${pathTotemFiles}/"${output_yED}.smb" > ${pathTotemFiles}/input.smb
echo -e "\nHOARE \n \nPRE : {$1} \n \nTRACE : $2; \n \nPOST : {$3} \n \nEND" >> ${pathTotemFiles}/input.smb

##Execution of Totembionet with smb input file
./${pathTotem} -csv ${pathTotemFiles}/input.smb &> /dev/null

##Extraction of the number of model that satisfy wp(Perm), from .out file. Result stored in modelNumber.txt. 
sed -n '/Total number of models/p' ${pathTotemFiles}/input.out | cut -c26- > modelNumber.txt

satisfied_models=$(($(cat modelNumber.txt)))
total_number=$(sed -n '/Number of effective parameterizations/p' "$pathTotemFiles/input.out" | cut -d ":" -f2 | tr -d " ")

i=0

if [[ ($total_number -ne "") && ($satisfied_models -eq $smaller_models) ]];
then
i=$((i+1))
cat ${pathTotemFiles}/input.out > "$pathTotemFiles/input_$i.out"
printf "Several output with same number of models\n"
fi

if [[ ($total_number -ne "") && ($satisfied_models -lt $total_number) ]];
then
cat ${pathTotemFiles}/input.out > $pathTotemFiles/input.out
printf "Identification of parameters\n"
smaller_models=$satisfied_models
fi

if [[ ($total_number -ne "") && ($satisfied_models -lt $smaller_models) ]];
then
cat ${pathTotemFiles}/input.out > $pathTotemFiles/input.out
smaller_models=$satisfied_models
printf "Identification of parameters\n"
fi

##Some remarks: 

# We consider that there is no CTL formula so we check only the result of the enumeration of model satisfying wp. 
# All the knowledge about K parameters are encoded in PARA block directly. 
