(***
Implémentation du calcul de plus faible précondition à l'aide de logique de Hoare
sur des réseaux de régulation biologique.

@author Maxime Folschette

Référence :
  G. Bernot, J.-P. Comet, Z. Khalis, A. Richard, O. Roux,
  A genetically modified Hoare logic,
  Theoretical Computer Science,
  2018.
  ISSN: 0304-3975
  DOI: https://doi.org/10.1016/j.tcs.2018.02.003

Cf. documentation du fichier README

Principe :
  Définit des langages d'arbres pour représenter des propriétés sur les réseaux
  (formules du type « a = 1 ET b > 0 OU ... »)
  et un langage impératif simple (incrémentation, conditionnelle, boucles, et quantificateurs)
  dans l'objectif de calculer une plus faible précondition à partir d'une postcondition
  et d'un programme donnés, sur un réseau donné.
  La plus faible précondition peut aussi être exportée en Answer Set Programming (ASP)
  pour être résolue avec Clingo 3.

Utilisation :
  Le modèle doit être modifié « en dur » dans la section « Informations du BRN ».
  Les résultats (calcul de plus faible précondition, simplifications, etc.) sont à écrire
  à la fin du programme dans les sections « Bac à sable ».
  Un exemple est déjà implémenté (quelques applications tirés du papier de référence).
  Le programme est fonctionnel et peut être exécuté par la commande :
    ocaml main.ml

Limitations théoriques :
  * Il faut pour le moment définir un invariant explicite pour les boulces (Tant que)
  * Les pré-conditions de boucles utilisent des sous-formules à part
      (avec des variables « fraiches ») qui ne sont pas impactées par la simplification
Limitations techniques :
  * Le modèle et les traitements doivent être implémentés « en dur » dans le code
  * La résolution par Clingo peut être très longue pour certaines formules
  * La sortie de Clingo peut être difficile à lire

Fonctions utiles :
  - wp : calcule la plus faible précondition associée à une postcondition et un programme donnés
  - simplify : simplifie une formule de type précondition ou postcondition, grâce
        à des propriétés logiques simples comme les règles de De Morgan
  - string_of_formula : traduit une formule en chaîne de caractères affichable
  - string_of_prog_indent : idem pour un programma impératif (avec indentations)
  - write_example : traduit une formule en ASP et l'écrit dans un fichier
  - asp_params : affiche la correspondance entre les variables ASP et les paramètres du modèle

TODO :

  * « Purifier » les fonctions
  * Faire un calcul automatique de plus faible invariant pour les boulces
  * Terminer les traitements des FreshState (simplifications, etc.)
  * Passer à Clingo 5
***)



(****************************)
(*** Fonction utilitaires ***)
(****************************)

open List ;;

(** Fonction identité **)
let id x = x ;;

(** Conversion d'une liste en chaîne de caractères selon une fonction de transformation et un séparateur *)
let rec string_of_list conv sep l =
  match l with
    | [] -> ""
    | h :: [] -> (conv h)
    | h :: t -> ((conv h) ^ sep ^ (string_of_list conv sep t)) ;;

let string_of_list_delim conv sep delim1 delim2 l =
  delim1 ^ string_of_list conv sep l ^ delim2 ;;

(** Calcul de l'ensemble des parties *)
let powerset l =
  let rec ps lp l =
    match l with
      | [] -> [lp]
      | h :: t -> (ps lp t) @ (ps (lp @ [h]) t) in
  ps [] l ;;

(** Calcul de l'ensemble des parties ordonné *)
let powerset_sort l fcompare =
  let rec ps lp l fcompare =
    match l with
      | [] -> [sort fcompare lp]
      | h :: t -> (ps lp t fcompare) @ (ps (lp @ [h]) t fcompare) in
  ps [] l fcompare ;;

(** Récupération de l'index d'un élément d'une liste *)
let index l a =
  let rec index_rec l a n =
    match l with
      | [] -> raise Not_found
      | h :: t -> if h = a then n else index_rec t a (n+1) in
  index_rec l a 0 ;;



(**************************)
(*** Opérateurs communs ***)
(**************************)

(** Opérateurs logiques *)
type propop2 =
  | And | Or | Impl ;;
type propop1 =
  | Neg ;;
type propop0 =
  | True | False ;;

(** Opérateurs logiques et arithmétiques *)
type relop2 =
  | LE | LT | GE | GT | Eq | NEq ;;
type exprop2 =
  | Plus | Minus ;;

(** Conversion des opérateurs en chaînes de caractères pour ASP *)
let asp_of_relop2 = function
  | LE -> "<="
  | LT -> "<"
  | GE -> ">="
  | GT -> ">"
  | Eq -> "=="
  | NEq -> "!=" ;;
let asp_of_exprop2 = function
  | Plus -> "+"
  | Minus -> "-" ;;

(** Conversion des opérateurs en chaînes de caractères pour affichage *)
let string_of_propop2 = function
  | And -> "&"
  | Or -> "|"
  | Impl -> "->" ;;
let string_of_propop1 = function
  | Neg -> "!" ;;
let string_of_propop0 = function
  | True -> "true"
  | False -> "false" ;;
let string_of_relop2 = function
  | LE -> "<="
  | LT -> "<"
  | GE -> ">="
  | GT -> ">"
  | Eq -> "="
  | NEq -> "!=" ;;
let string_of_exprop2 = function
  | Plus -> "+"
  | Minus -> "−" ;;



(**********************)
(*** Types d'un BRN ***)
(**********************)

(** Types **)
type var = string ;;
type mult = string ;;
type varmax = int ;;
type varpredec = mult list ;;

(** Fonctions de comparaison **)
let varequals = (=) ;;
let multcompare = compare ;;



(**************************************************)
(*** Grammaire pour les formules de multiplexes ***)
(**************************************************)

(** Expression et formules de multiplexes *)
type multexpr =
  | MExprBin of exprop2 * multexpr * multexpr
  | MExprConst of int ;;
type multformula =
  | MPropConst of propop0
  | MPropUn of propop1 * multformula
  | MPropBin of propop2 * multformula * multformula
  | MRel of relop2 * multexpr * multexpr
  | MAtom of var * int
  | MMult of mult ;;

let vars = 
[("sk" ,( 2, [ "noASK" ;"rp" ;"noEn1" ;"restrictGF" ]));
("a" ,( 2, [ "skACT" ;"restrictSK" ;"endREP" ;"noCI" ]));
("b" ,( 2, [ "noCI" ;"SAC" ;"noWEE1" ;"aACT2" ]));
("ep" ,( 1, [ "bACT2" ;"noEn2" ]));
("en" ,( 1, [ "noA" ;"noSK" ;"noB" ]))];; 

let mults = 
[ ("noASK", MPropUn(Neg, MAtom("a", 1 ) ) );
 ("rp", MAtom("sk", 1 ) );
 ("noEn1", MPropUn(Neg, MAtom("en", 1 ) ) );
 ("restrictGF", MPropBin(And, MAtom("gf", 1 ), MPropUn(Neg, MAtom("b", 2 ) ) ) );
 ("skACT", MAtom("sk", 2 ) );
 ("restrictSK", MPropBin(And, MAtom("sk", 1 ), MAtom("a", 1 ) ) );
 ("endREP", MPropBin(And, MPropUn(Neg, MAtom("sk", 1 ) ), MAtom("a", 1 ) ) );
 ("noCI", MPropUn(Neg, MPropBin(Or, MAtom("en", 1 ), MAtom("ep", 1 ) ) ) );
 ("SAC", MPropBin(And, MAtom("ep", 1 ), MAtom("a", 2 ) ) );
 ("noWEE1", MAtom("b", 1 ) );
 ("aACT2", MAtom("a", 2 ) );
 ("noA", MPropUn(Neg, MAtom("a", 1 ) ) );
 ("noSK", MPropUn(Neg, MAtom("sk", 1 ) ) );
 ("noB", MPropUn(Neg, MAtom("b", 1 ) ) );
 ("bACT2", MAtom("b", 2 ) );
 ("noEn2", MPropUn(Neg, MAtom("en", 1 ) ) )];; 
(** @author Maxime Folschette **)

(****************************)
(*** Fonctions ajustables ***)
(****************************)

(** Fonctions de conversion **)
let asp_of_var = String.capitalize_ascii ;;
let asp_of_mult = String.capitalize_ascii ;;
let string_of_var = id ;;
let string_of_mult = id ;;
(*
(* Fonction de conversion personnalisée *)
let string_of_mult = function
  | "l" -> "l"
  | "lambda" -> "lamb"
  | "sigma" -> "sig" ;;
*)

(** Extraction des variables, multiplexes, etc **)
let varlist = fst (split vars) ;;
let multlist = fst (split mults) ;;
let getbound a = fst (assoc a vars) ;;
let getpredec a = snd (assoc a vars) ;;
let getformula m = assoc m mults ;;

(** Conversion des expressions et formules de multiplexes en chaînes pour affichage *)
let rec string_of_multexpr = function
  | MExprBin(op, e1, e2) -> ("(" ^ (string_of_multexpr e1) ^ (string_of_exprop2 op) ^ (string_of_multexpr e2) ^ ")")
  | MExprConst(i) -> string_of_int i ;;
let rec string_of_multformula = function
  | MPropConst(op) -> string_of_propop0 op
  | MPropUn(op, f) -> ((string_of_propop1 op) ^ (string_of_multformula f))
  | MPropBin(op, f1, f2) -> ("(" ^ (string_of_multformula f1) ^ " " ^ (string_of_propop2 op) ^ " " ^ (string_of_multformula f2) ^ ")")
  | MRel(op, e1, e2) -> ("(" ^ (string_of_multexpr e1) ^ (string_of_relop2 op) ^ (string_of_multexpr e2) ^ ")")
  | MAtom(v, i) -> ((string_of_var v) ^ "_" ^ (string_of_int i))
  | MMult(m) -> string_of_mult m ;;



(************************************************************************)
(*** Grammaire et sémantique pour les formules de la logique de Hoare ***)
(************************************************************************)

(** Expressions et formules pour les triplets de Hoare et les conditions de programme *)
type expr =
  | ExprBin of exprop2 * expr * expr
  | ExprVar of var
  | ExprParam of var * (mult list)
(*| ExprVal of string (* TODO: Variables extérieures au triplet de Hoare ? *) *)
  | ExprConst of int ;;
type formula =
  | PropConst of propop0
  | PropUn of propop1 * formula
  | PropBin of propop2 * formula * formula
  | Rel of relop2 * expr * expr
  | FreshState of formula ;;

(** Conversion d'une formule de multiplexe en formule FOL générale *)
let formula_of_matom v i = Rel(GE, ExprVar v, ExprConst i) ;;
let rec expr_of_multexpr = function
  | MExprBin(op, e1, e2) -> ExprBin(op, expr_of_multexpr e1, expr_of_multexpr e2)
  | MExprConst(i) -> ExprConst(i) ;;
let rec formula_of_multformula = function
  | MPropConst(op) -> PropConst(op)
  | MPropUn(op, f) -> PropUn(op, formula_of_multformula f)
  | MPropBin(op, f1, f2) -> PropBin(op, formula_of_multformula f1, formula_of_multformula f2)
  | MRel(op, e1, e2) -> Rel(op, expr_of_multexpr e1, expr_of_multexpr e2)
  | MAtom(v, i) -> formula_of_matom v i
  | MMult(m) -> formula_of_multformula (getformula m) ;;

(** Conversion d'une expression en chaîne de caractères pour ASP *)
let rec asp_of_state_expr s =
  let paramlist = fold_right (fun v l -> (powerset_sort (getpredec v) multcompare) :: l) varlist [] in
function
  | ExprBin(op, e1, e2) -> ("(" ^ (asp_of_state_expr s e1) ^ (asp_of_exprop2 op) ^ (asp_of_state_expr s e2) ^ ")")
  | ExprVar(v) -> (if s = 0 then "" else "S" ^ (string_of_int s) ^ "X") ^ (asp_of_var v)
  | ExprParam(v, l) -> ("K_" ^ asp_of_var v ^ "_" ^ (string_of_int (index (nth paramlist (index varlist v)) (sort multcompare l))))
  | ExprConst(i) -> string_of_int i ;;
let asp_of_expr = asp_of_state_expr 0 ;;

(** Conversion d'une expression en chaîne de caractères pour affichage *)
let rec string_of_expr = function
  | ExprBin(op, e1, e2) -> ("(" ^ (string_of_expr e1) ^ (string_of_exprop2 op) ^ (string_of_expr e2) ^ ")")
  | ExprVar(v) -> string_of_var v
  | ExprParam(v, l) -> (if l=[] then "K_" ^ string_of_var v else "K_" ^ string_of_var v ^ ":" ^ (string_of_list string_of_mult ":" l) )
  | ExprConst(i) -> string_of_int i ;;
let rec string_of_formula = function
  | PropConst(op) -> string_of_propop0 op
  | PropUn(op, f) -> ((string_of_propop1 op) ^ (string_of_formula f))
  | PropBin(op, f1, f2) -> ("(" ^ (string_of_formula f1) ^ " " ^ (string_of_propop2 op) ^ " " ^ (string_of_formula f2) ^ ")")
  | Rel(op, e1, e2) -> ("(" ^ (string_of_expr e1) ^ (string_of_relop2 op) ^ (string_of_expr e2) ^ ")")
  | FreshState(f) -> ("forall state sigm, " ^ (string_of_formula f)) ;;

(** Substitution d'une expression à une variable *)
let replace f v e =
  let rec replace_expr ex v' e' =
    match ex with
      | ExprBin(op, e1, e2) -> ExprBin(op, replace_expr e1 v' e', replace_expr e2 v' e')
      | ExprVar(v) -> if (varequals v v') then e' else ExprVar(v)
      | ExprParam(v, l) -> ExprParam(v, l)
      | ExprConst(i) -> ExprConst(i) in
  let rec replace_formula tree v' e' =
    match tree with
      | PropConst(op) -> PropConst(op)
      | PropUn(op, f) -> PropUn(op, replace_formula f v' e')
      | PropBin(op, f1, f2) -> PropBin(op, replace_formula f1 v' e', replace_formula f2 v' e')
      | Rel(op, e1, e2) -> Rel(op, replace_expr e1 v' e', replace_expr e2 v' e')
      | FreshState(f) -> FreshState(f) in
  replace_formula f v e ;;

(** Simplification d'une formule selon la valeur de certaines variables et certains paramètres *)
type assoc_option =
  | Found of int
  | Not_found ;;

(* Simplification d'une expression *)
let rec simpl_expr e lv lk =
  let rec assoc_find eq k1 l =
    match l with
      | [] -> Not_found
      | (k2, v) :: t -> match eq k1 k2 with
                          | true -> Found v
                          | false -> assoc_find eq k1 t in
  let paramequals = (=) in
  let evalfun_of_exprop2 = function
    | Plus -> (+)
    | Minus -> (-) in
  match e with
    | ExprBin(op, e1, e2) -> (match simpl_expr e1 lv lk, simpl_expr e2 lv lk with
                                | ExprConst(i1), ExprConst(i2) -> ExprConst(evalfun_of_exprop2 op i1 i2)
                                | es1, es2 -> ExprBin(op, es1, es2))
    | ExprVar(v) -> (match assoc_find varequals v lv with
                       | Found i -> ExprConst(i)
                       | Not_found -> e)
    | ExprParam(v, l) -> (match assoc_find paramequals (v, l) lk with
                            | Found i -> ExprConst(i)
                            | Not_found -> e)
    | ExprConst(i) -> ExprConst(i) ;;

let rec simpl_formula f lv lk =
  let evalfun_of_relop2 = function
    | LE -> (<=)
    | LT -> (<)
    | GE -> (>=)
    | GT -> (>)
    | Eq -> (=)
    | NEq -> (!=) in
  let simpl_exprop2 op e1 e2 lv lk =
    match (simpl_expr e1 lv lk), (simpl_expr e2 lv lk) with
      | ExprConst(i1), ExprConst(i2) -> (match (evalfun_of_relop2 op) i1 i2 with
                                           | true -> PropConst(True)
                                           | false -> PropConst(False))
      | es1, es2 -> Rel(op, es1, es2) in
(* Simplification d'une formule *)
    match f with
      | PropConst(op) -> PropConst(op)
      | PropUn(op, f) -> simpl_propop1 op f lv lk
      | PropBin(op, f1, f2) -> simpl_propop2 op f1 f2 lv lk
      | Rel(op, e1, e2) -> simpl_exprop2 op e1 e2 lv lk
      | FreshState(f) -> FreshState(f) and
  (* TODO: Propager la simplification dans les FreshState (déjà sans raffinement) *)
  (* TODO: Réfléchir à la simplification de FreshState : simplification des paramètres ? *)
(* Simplifications atomiques pour chaque forme de proposition *)
(* Opérateur unaire *)
simpl_propop1 op f lv lk =
  match op with
    | Neg -> match simpl_formula f lv lk with
               | PropConst True -> PropConst False
               | PropConst False -> PropConst True
               | PropUn(Neg, ffs) -> ffs
               | fs -> PropUn(Neg, fs) and
(* Opérateurs binaires *)
simpl_propop2 op f1 f2 lv lk =
  match op with
    | And -> simpl_propop2_And f1 f2 lv lk
    | Or -> simpl_propop2_Or f1 f2 lv lk
    | Impl -> simpl_propop2_Impl f1 f2 lv lk and
simpl_propop2_And f1 f2 lv lk =
  match simpl_formula f1 lv lk with
    | PropConst(True) -> simpl_formula f2 lv lk
    | PropConst(False) -> PropConst(False)
    | fs1 -> (match simpl_formula f2 lv lk with
                | PropConst(True) -> fs1
                | PropConst(False) -> PropConst(False)
                | fs2 -> PropBin(And, fs1, fs2)) and
simpl_propop2_Or f1 f2 lv lk =
  match simpl_formula f1 lv lk with
    | PropConst(True) -> PropConst(True)
    | PropConst(False) -> simpl_formula f2 lv lk
    | fs1 -> (match simpl_formula f2 lv lk with
                | PropConst(True) -> PropConst(True)
                | PropConst(False) -> fs1
                | fs2 -> PropBin(Or, fs1, fs2)) and
simpl_propop2_Impl f1 f2 lv lk =
  match simpl_formula f1 lv lk with
    | PropConst(True) -> simpl_formula f2 lv lk
    | PropConst(False) -> PropConst(True)
    | fs1 -> (match simpl_formula f2 lv lk with
                | PropConst(True) -> PropConst(True)
                | PropConst(False) -> PropUn(Neg, fs1)
                | fs2 -> PropBin(Impl, fs1, fs2)) ;;

(** Retourne la formule simplifiée et les hypothèses de simplification sous forme de formule, séparément *)
(* TODO: Traduire à l'aide d'une fonction xx_of_list (fold_left ?) instpirée de string_of_list *)
let couple_simplify f lv lk =
  let rec iter_lv l =
    match l with
      | [] -> PropConst(True)
      | (v, i) :: [] -> Rel(Eq, ExprVar(v), ExprConst(i))
      | (v, i) :: t -> PropBin(And,  Rel(Eq, ExprVar(v), ExprConst(i)), iter_lv t) in
  let rec iter_lk l =
    match l with
      | [] -> PropConst(True)
      | ((v, lm), i) :: [] -> Rel(Eq, ExprParam(v, lm), ExprConst(i))
      | ((v, lm), i) :: t -> PropBin(And,  Rel(Eq, ExprParam(v, lm), ExprConst(i)), iter_lk t) in
  (simpl_formula (PropBin(And, iter_lv lv, iter_lk lk)) [] [], simpl_formula f lv lk) ;;

(** Retourne la formule simplifiée et les hypothèses de simplification en conjonction *)
let simplify f lv lk =
  let (a, b) = couple_simplify f lv lk in
    PropBin(And, a, b) ;;





(*****************************************************************)
(*** Traduction d'une formule en programme ASP pour résolution ***)
(*****************************************************************)

(** pas utilisé par TotemBioNet **)

(** Traduction d'une formule en ASP *)
let write_asp out f =
  let rec power x i = if i = 0 then 1 else x * power x (i - 1) in
  let rec from_up_to n m = if n > m then [] else n :: (from_up_to (n + 1) m) in
  let asp_of_state_var s v = (if s = 0 then "" else "S" ^ (string_of_int s) ^ "X") ^ (asp_of_var v) in
  let asp_of_state_param p = let (v, n) = p in "K_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) in
  let param_enum_var v = fold_right (fun n l -> (v, n) :: l) (from_up_to 0 ((power 2 (length (getpredec v))) - 1)) [] in
  let param_enum = fold_right (fun v l -> (param_enum_var v) @ l) varlist [] in
  let state_enum s = ((if s = 0 then "main_" else "") ^ "state(" ^ (string_of_list (asp_of_state_var s) "," varlist) ^ ")") in
  let soft_state_enum s = if s = 0 then "" else ((state_enum s) ^ ", ") in
  let params_enum = ("params(" ^ (string_of_list asp_of_state_param "," param_enum) ^ ")") in
  let iatom = ref (-1) in
  let nextatom s = iatom := !iatom + 1 ; "prop" ^ (string_of_int !iatom) ^
                                          (if s = 0 then "" else "(" ^ (string_of_list (asp_of_state_var s) "," varlist) ^ ")") in
  let istate = ref (0) in
  let nextstate () = istate := !istate + 1 ; !istate in
  let rec write_atom_state out f s =
    match f with
      | PropConst(op) ->
        (match op with
          | True -> "t"
          | False -> "f")
      | PropUn(op, f1) ->
        (let ncurrent = nextatom s in
        let nf1 = write_atom_state out f1 s in
          (match op with
            | Neg -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ "not " ^ nf1 ^ ".\n")
          ) ; ncurrent)
      | PropBin(op, f1, f2) -> (
        let ncurrent = nextatom s in
        let nf1 = write_atom_state out f1 s in
        let nf2 = write_atom_state out f2 s in
          (match op with
            | And -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf1 ^ ", " ^ nf2 ^ ".\n")
            | Or -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf1 ^ ".\n" ^
                                       ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf2 ^ ".\n")
            | Impl -> output_string out (ncurrent ^ " :- " ^ (soft_state_enum s) ^ nf2 ^ ".\n" ^
                                         ncurrent ^ " :- " ^ (soft_state_enum s) ^ "not " ^ nf1 ^ ".\n")
          ) ; ncurrent)
      | Rel(op, e1, e2) ->
        (let ncurrent = nextatom s in
          output_string out (ncurrent ^ " :- " ^ (state_enum s) ^ ", " ^ params_enum ^ ", " ^
                             (asp_of_state_expr s e1) ^ " " ^ (asp_of_relop2 op) ^ " " ^ (asp_of_state_expr s e2) ^ ".\n")
          ; ncurrent)
      | FreshState(f) ->
        (let ncurrent = nextatom s in
        let nf1 = nextatom s in
        let ns = nextstate () in
        let nf2 = write_atom_state out f ns in
          output_string out (ncurrent ^ " :- not " ^ nf1 ^ ".\n") ;
          output_string out (nf1 ^ " :- " ^ (state_enum ns) ^ ", not " ^ nf2 ^ ".\n")
          ; ncurrent) in
  let write_atoms out f =
    output_string out ("main_state :- " ^ write_atom_state out f 0 ^ ".\n") ;
    output_string out "\nt.\n:- f.\n\n:- not main_state.\n" in
  let write_header out =
    iter (fun v -> output_string out ("varbound_" ^ (asp_of_var v) ^ "(0.." ^ (string_of_int (getbound v)) ^ ").\n")) varlist ;
    output_string out "\n" ;
    iter (fun v -> output_string out ("1 {var_" ^ (asp_of_var v) ^ "(X) : varbound_" ^ (asp_of_var v) ^ "(X)} 1.\n")) varlist ;
    output_string out "\n" ;
    iter (fun v ->
      (iter
        (fun n -> output_string out ("1 {param_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) ^ "(X) : varbound_" ^ (asp_of_var v) ^ "(X)} 1.\n"))
        (from_up_to 0 ((power 2 (length (getpredec v))) - 1))
      )) varlist ;
    output_string out "\n" ;

    output_string out ((state_enum 0) ^ " :- ") ;
    output_string out (string_of_list (fun v -> "var_" ^ (asp_of_var v) ^ "(" ^ (asp_of_var v) ^ ")") ", " varlist) ;
    output_string out ".\n\n" ;

    output_string out ("state(" ^ (string_of_list asp_of_var "," varlist) ^ ") :- ") ;
    output_string out (string_of_list (fun v -> "varbound_" ^ (asp_of_var v) ^ "(" ^ (asp_of_var v) ^ ")") ", " varlist) ;
    output_string out ".\n\n" ;

    output_string out (params_enum ^ " :- ") ;
    output_string out (string_of_list
      (fun c -> let (v, n) = c in "param_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) ^ "(K_" ^ (asp_of_var v) ^ "_" ^ (string_of_int n) ^ ")")
      ", " param_enum) ;
    output_string out ".\n\n" in
  let write_footer out =
    output_string out ("\n#hide.\n#show main_state/" ^ (string_of_int (length varlist)) ^
                       ".\n#show params/" ^ (string_of_int (length param_enum)) ^ ".\n\n") in
  write_header out ; write_atoms out f ; write_footer out ;;

(** Écriture dans un fichier *)
let write_example f file =
  let out = open_out file in
(*  write_asp stdout f ;  (* Affichage à l'écran *) *)
    write_asp out f ;
    close_out out ;;

(** Affichage des correspondances entre paramètres et variables ASP *)
let asp_params () =
  let paramlist =
    fold_right (fun v l -> (powerset_sort (getpredec v) multcompare) :: l) varlist [] in
  iter (fun v ->
    iter (fun lp -> print_endline ((string_of_expr (ExprParam(v, lp))) ^ " ..... " ^ (asp_of_expr (ExprParam(v, lp)))))
    (nth paramlist (index varlist v))) varlist ;;
(*  iter (fun n -> print_string ((string_of_expr (ExprParam(fst n, snd n))) ^ " ..... " ^ (asp_of_expr (ExprParam(fst n, snd n))))) paramlist ;; *)





(************************************************)
(*** Grammaire pour les programmes impératifs ***)
(************************************************)

(** Grammaire des programmes impératifs *)
type prog =
  | Skip
  | Set of var * int
  | Incr of var
  | Decr of var
  | Seq of prog * prog
  | If of formula * prog * prog
  | While of formula * formula * prog
  | Forall of prog * prog
  | Exists of prog * prog
  | Assert of formula ;;

(** Conversion d'un programme impératif en chaîne de caractères pour affichage avec indentations *)
let string_of_prog_indent p =
  let nli indent = ("\n" ^ (String.make indent ' ')) in
  let newindent = 2 in
  let rec string_of_prog_indentation p indent =
    match p with
      | Skip -> "SKIP"
      | Incr(v) -> ((string_of_var v) ^ "+")
      | Decr(v) -> ((string_of_var v) ^ "-")
      | Set(v, k) -> ((string_of_var v) ^ " := " ^ (string_of_int k))
      | Seq(p1, p2) -> ((string_of_prog_indentation p1 indent) ^ " ;"
        ^ (nli indent) ^ (string_of_prog_indentation p2 indent))
      | If(c, p1, p2) -> ("IF " ^ (string_of_formula c) ^ " THEN"
        ^ (nli (indent + newindent)) ^ (string_of_prog_indentation p1 (indent + newindent))
        ^ (nli indent) ^ "ELSE"
        ^ (nli (indent + newindent)) ^ (string_of_prog_indentation p2 (indent + newindent))
        ^ (nli indent) ^ "END IF")
      | While(c, i, p) -> ("WHILE " ^ (string_of_formula c) ^ " WITH " ^ (string_of_formula i) ^ " DO"
        ^ (nli (indent + newindent)) ^ (string_of_prog_indentation p (indent + newindent))
        ^ (nli indent) ^ "END WHILE")
      | Forall(p1, p2) -> ("∀(" ^ (string_of_prog_indentation p1 indent) ^ ", " ^ (string_of_prog_indentation p2 indent) ^ ")")
      | Exists(p1, p2) -> ("∃(" ^ (string_of_prog_indentation p1 indent) ^ ", " ^ (string_of_prog_indentation p2 indent) ^ ")")
      | Assert(f) -> ("assert(" ^ (string_of_formula f) ^ ")") in
string_of_prog_indentation p 0 ;;

(** Conversion d'un programme impératif en chaîne de caractères pour affichage en ligne *)
let rec string_of_prog = function
  | Skip -> "SKIP"
  | Incr(v) -> ((string_of_var v) ^ "+")
  | Decr(v) -> ((string_of_var v) ^ "−")
  | Set(v, k) -> ((string_of_var v) ^ " := " ^ (string_of_int k))
  | Seq(p1, p2) -> ((string_of_prog p1) ^ " ; " ^ (string_of_prog p2))
  | If(c, p1, p2) -> ("(IF " ^ (string_of_formula c) ^ " THEN " ^ (string_of_prog p1) ^ " ELSE " ^ (string_of_prog p2) ^ " END IF)")
  | While(c, i, p) -> ("(WHILE " ^ (string_of_formula c) ^ " WITH " ^ (string_of_formula i) ^ " DO " ^ (string_of_prog p) ^ " END WHILE)")
  | Forall(p1, p2) -> ("∀(" ^ (string_of_prog p1) ^ ", " ^ (string_of_prog p2) ^ ")")
  | Exists(p1, p2) -> ("∃(" ^ (string_of_prog p1) ^ ", " ^ (string_of_prog p2) ^ ")")
  | Assert(f) -> ("assert(" ^ (string_of_formula f) ^ ")") ;;



(*******************************************************)
(*** Calcul de prédicats : plus faible pré-condition ***)
(*******************************************************)

(** Formules de type PHI *)
let phi v omega =
  let rec phi_eval v l omega =
    match l with
      | [] -> PropConst True
      | h :: [] -> if (mem h omega)
                   then formula_of_multformula (getformula h)
                   else PropUn(Neg, formula_of_multformula (getformula h))
      | h :: t -> PropBin(And, (if (mem h omega)
                                then formula_of_multformula (getformula h)
                                else PropUn(Neg, formula_of_multformula (getformula h))),
                               phi_eval v t omega) in
  phi_eval v (getpredec v) omega ;;
let rec conj_phi_param v l comp =
  match l with
    | [] -> raise (Invalid_argument "conj_phi_param: Empty powerset (should contain at least [])")
    | h :: [] -> PropBin(Impl, phi v h, Rel(comp, ExprParam(v, h), ExprVar v))
    | h :: t -> PropBin(And, PropBin(Impl, phi v h, Rel(comp, ExprParam(v, h), ExprVar v)), conj_phi_param v t comp) ;;
let phi_incr v = conj_phi_param v (powerset (getpredec v)) GT ;;
let phi_decr v = conj_phi_param v (powerset (getpredec v)) LT ;;

(** Plus faible pré-condition *)
let incr_formula v = ExprBin(Plus, ExprVar v, ExprConst 1) ;;
let decr_formula v = ExprBin(Minus, ExprVar v, ExprConst 1) ;;
let rec wp prog post =
  match prog with
    | Skip -> post
    | Incr v -> PropBin(And, phi_incr v, replace post v (incr_formula v))
    | Decr v -> PropBin(And, phi_decr v, replace post v (decr_formula v))
    | Set(v, k) -> replace post v (ExprConst(k))
    | Seq(p1, p2) -> wp p1 (wp p2 post)
    | If(c, p1, p2) -> PropBin(And, PropBin(Impl, c, wp p1 post),
                                     PropBin(Impl, PropUn(Neg, c), wp p2 post))
    | While(c, i, p) -> PropBin(And, i, PropBin(And, FreshState(PropBin(Impl, PropBin(And, i, c), wp p i)),
                                                      FreshState(PropBin(Impl, PropBin(And, i, PropUn(Neg, c)), post))))
    | Forall(p1, p2) -> PropBin(And, wp p1 post, wp p2 post)
    | Exists(p1, p2) -> PropBin(Or, wp p1 post, wp p2 post)
    | Assert(f) -> PropBin(And, f, post) ;;

let prog = Forall(Forall(Forall(Forall(Forall(Forall(Forall(Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "a" ), Decr "b" ), Decr "b" ), Incr "en" ), Decr "ep" ) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "a" ), Decr "b" ), Decr "b" ), Decr "ep" ), Incr "en" )) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "a" ), Decr "b" ), Decr "ep" ), Decr "b" ), Incr "en" )) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "b" ), Decr "a" ), Decr "b" ), Incr "en" ), Decr "ep" )) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "b" ), Decr "a" ), Decr "b" ), Decr "ep" ), Incr "en" )) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "b" ), Decr "a" ), Decr "ep" ), Decr "b" ), Incr "en" )) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "b" ), Decr "b" ), Decr "a" ), Decr "ep" ), Incr "en" )) , Seq( Seq( Seq( Seq( Seq( Seq( Seq( Incr "b", Incr "ep" ), Decr "a" ), Decr "b" ), Decr "b" ), Decr "a" ), Incr "en" ), Decr "ep" ));;

let post = PropBin(And, PropBin(And, PropBin(And, PropBin(And, PropBin(And, Rel( Eq, ExprVar "sk", ExprConst 0 ), Rel( Eq, ExprVar "ep", ExprConst 0 )), Rel( Eq, ExprVar "a", ExprConst 0 )), Rel( Eq, ExprVar "b", ExprConst 0 )), Rel( Eq, ExprVar "en", ExprConst 1 )), Rel( Eq, ExprVar "gf", ExprConst 1 ));; 

let wps =  simplify (wp prog post) [( "sk", 0);( "ep", 0);( "a", 2);( "b", 1);( "en", 0);( "gf", 1)] [];; 

print_endline (string_of_formula wps) ;; 

