#!/usr/bin/env bash

################ Util remarks: ################ 
#$1 : a mono-state of Thomas' TG. E.G : sk=0,ep=0,a=0,b=0,en=1,p53=0,gf=1
#$2 : variable of a GRBM
#<<< pass a string instead of a file. 
# {N,} : at least n times and taken in charge supported by extended grep (egrep).

################ Function definitions: ################ 
function resources {
r1=$(egrep "=> * $v1 *" $file_path)
r2=$(egrep "=> [A-Za-z]* $v1 *" $file_path) #var 1a, _a, cyc1 (mêmes règles totem) FLAG
rtot="$r1 $r2" #FLAG => a b was a problem 
cut -d ";"  --output-delimiter=" " -f 1- <<< $(tr -d " " <<< $rtot)
#$1 is a RRG variable (str). 
} 

function get_formula {
# extract the formula associated with the corresponding multiplex:
ress=$(tr -d "[ ]" <<< $(cut -d "]" -f1  <<< $(cut -d "[" -f2 <<< $1)))
# get rid of useful parenthesis.
echo $(sed 's/^.\(.*\).$/\1/' <<< $ress)
#$1 is of the form multiplex_name[multiplex_formula]=>multiplex_target;
}

function get_var_state {
# get the state of a variable ($2) in a given state ($1).
cut -d "=" -f2 <<< $(grep -o "$2=[0-9]" <<< $1)
}

function fill_ress_set {
ress_name=$(cut -d "[" -f1 <<< $1)
ress_set="$ress_set $ress_name"
}

################ Some Assignments: ################ 
file_path="./TotemFiles/input.out"
v1=$2 #v1 the only variable involved into esRequis specification. 
applicable_K="K_$v1"
ress_set=""

################ Main: ################
for line in $(resources $v1)
	do 
	form=$(get_formula $line) 
	#SIMPLIFICATION : the multiplex formula ($form) must be of the form: "atom_v op atom_w ... " where v and w are GRBM variables such that v != w and op belong in {or,and}. 
	vars=$(egrep -o "\b[A-Za-z]{1,}[0-9]*" <<< $form)
	for var in $vars
		do
		val_eta=$(get_var_state $1 $var) # state of var in $1
		form=`echo $form|sed "s/$var/$val_eta/g"` #var is replaced by its value in the state $1
		done 
	is_ress=$(($form)) #evaluation of the formula. 
	# is_ress >0 is true and =0 is false.
	if [ "$is_ress" == "1" ];
		then 
		fill_ress_set $line # a resource is added to the set of resources if its associated formula is true in $1. 
	fi
done
eta_var=$(get_var_state $1 $2) 
#sort ress_set to be able to compare with input.out in which resources are sorted by alphabetical order.
ress_set=$(echo $ress_set | tr ' ' '\n' | LC_COLLATE=C sort | tr '\n' ' ')
ress_set=$(sed "s/ /:/g" <<< $ress_set)

################ Outputs: ################ 
if [ "$ress_set" == "" ]
then 
	echo "$applicable_K"
else
	echo "$applicable_K:${ress_set::-1}"
fi

